CURRENT_APP=earth

all: $(CURRENT_APP)

run: $(CURRENT_APP)
	./$(CURRENT_APP)

cube: example
	./example cube

car: example
	./example gt86

clean:
	rm -rf build
	rm -f pymake/*.pyc
	rm -f Makefile.autogen
	rm -f data/models/*.objb
	rm -f data/textures/*.texb


# If we're cleaning - dont include generated files. Else they'll be generated
# just to be deleted immediately after. Not so much a speed issue when
# everything works, but is annoying during dev if augeneration is broken
# at some point.
ifneq (clean,$(MAKECMDGOALS))
include Makefile.autogen
endif

PYTHONS=make.py $(wildcard pymake/*.py) $(wildcard pymake_local/*.py)

pymake/.git:
	git submodule update --init --recursive

Makefile.autogen:  $(PYTHONS) pymake/.git
	@ECHO [PYMAKE] $@
	@./make.py

