#version 400

in vec4 normal;
out vec4 frag_colour;

const vec4 light = normalize(vec4(1,0,0,0));
const vec4 ambient = vec4(0.2, 0.2, 0.2, 1);

uniform vec4 base_colour;

void main () {
    float angle = clamp(dot(light, normal), 0, 1);
    frag_colour = ambient + base_colour * angle;
}
