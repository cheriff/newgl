#version 400

layout(location = 0 ) in vec4 vertex_position;
layout(location = 2 ) in vec2 texture_coordinate;
uniform mat4 view, projection;

out vec2 tex;

void main () {

    gl_Position = projection * view * vertex_position, 1.0;
    tex = texture_coordinate;
}
