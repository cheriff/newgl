#version 400

in vec2 tex;
out vec4 frag_colour;

uniform sampler2D base_texture;

void main () {
    frag_colour = texture(base_texture, tex);
}
