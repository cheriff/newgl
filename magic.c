#include <stdio.h>

// OBJb = 0x4f424a62
// MATb = 0x4d415462

char *str = "MATb";

int main(void)
{
    char *c;
    printf("%s = 0x", str);
    for (c=str; *c; c++) {
        printf("%02x", *c);
    }
    printf("\n");
}
