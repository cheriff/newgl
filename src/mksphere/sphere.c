#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include <argtable2.h>

typedef struct {
    float radius;
    float arcLength;
    int segmentsW;
    int segmentsH;
    FILE *outfile;
} sphere_info_t;

static sphere_info_t *
parse_sphere(int argc, char *argv[])
{
    struct arg_rex *literal = arg_rex1(NULL, NULL, "sphere", "<geometry>",  0, "\"sphere\"");
    struct arg_dbl *radius = arg_dbl0("r", "radius", "<r>", "Radius of sphere (Default: 1.0)");
    struct arg_dbl *arclen = arg_dbl0("a", "arclength", "<a>", "Arc Length (Default: 1.0)");
    struct arg_int *segments_w = arg_int1("w", "long", "<w>", "Longitudinal segments");
    struct arg_int *segments_h = arg_int1("h", "lat", "<h>", "lattudinal segments");
    struct arg_file *outfile = arg_file0("o", "output", "<outfile>", "output file (Default: stdout)");
    struct arg_end *end = arg_end(20);

    void *argtable[] =  {
        literal, radius, arclen, segments_w, segments_h, outfile, end,
    };

    radius->dval[0] = 1.0;
    arclen->dval[0] = 1.0;

    int r = arg_parse(argc, argv, argtable);
    if (r != 0) {
        arg_print_errors(stderr, end, argv[0]);
        arg_print_syntax(stderr, argtable, NULL);
        printf("\n");
        arg_print_glossary(stderr, argtable, "\t%-25s %s\n");
        return NULL;
    }

    static sphere_info_t ret;
    if (outfile->count == 0) {
        ret.outfile = stdout;
    } else {
        ret.outfile = fopen(outfile->filename[0], "w");
        if (!ret.outfile) {
            perror("Cannot open file");
            return NULL;
        }
    }

    ret.radius = (float)radius->dval[0];
    ret.arcLength = (float)arclen->dval[0];
    ret.segmentsH = segments_h->ival[0];
    ret.segmentsW = segments_w->ival[0];

    return &ret;
}

static int
emit_sphere(sphere_info_t *si)
{
    const float radius = si->radius;
    const float arcLength = si->arcLength;
    const int segmentsW = si->segmentsW;
    const int segmentsH = si->segmentsH;

    FILE *fout = si->outfile;

    fprintf(fout, "#Sphere:\n");
    fprintf(fout, "# Radius: %f:\n", radius);
    fprintf(fout, "# Arc Length: %f:\n", arcLength);
    fprintf(fout, "# Segments Width: %d:\n", segmentsW);
    fprintf(fout, "# Segments Height: %d:\n", segmentsH);
    fprintf(fout, "\n");

    int minj = (int)(segmentsH * (1-arcLength));

    uint32_t num_tris = (uint32_t)(2 * segmentsW + ((segmentsH-2) * segmentsW * 2));
    uint32_t num_vertices = (uint32_t)((segmentsH-minj+1) * (segmentsW+1));
    uint32_t num_indices = (uint32_t)num_tris * 3;

    fprintf(fout, "# Num Triangles: %d\n", num_tris);
    fprintf(fout, "# Num Vertices: %d\n", num_vertices);
    fprintf(fout, "# Num Indices: %d\n", num_indices);

    fprintf(fout, "\n");
    fprintf(fout, "g sphere\n");

    int i, j;

    for (j=minj; j<= segmentsH; j++) {
        float horangle = (float)M_PI*j/segmentsH;
        float z = radius * cosf(horangle);
        float ringradius = radius*sinf(horangle);

        for (i = 0; i <= segmentsW; i++) {
            float verangle = 2*(float)M_PI*i/segmentsW;

            float x = ringradius*cosf(verangle);
            float y = ringradius*sinf(verangle);


            //vertices.push(x, -z, y)
            fprintf(fout, "v %f %f %f\n", x, z, y);

            //_uvtData.push(i/_segmentsW, 1 - (j - minj)/(_segmentsH - minj), 1);
            fprintf(fout, "vt %f %f\n",
                    (float)(i)/(float)(segmentsW),
                    (float)(j - minj)/(float)(segmentsH - minj));

            // Normals of a sphere are just the position, normalised
            fprintf(fout, "vn %f %f %f\n",
                    x/radius,
                    -z/radius,
                    y/radius);

        }
    }

    for (j = 1; j <= segmentsH - minj; ++j) {
        for (i = 1; i <= segmentsW; ++i) {
            uint32_t a = (uint32_t)((segmentsW + 1)*j + i);
            uint32_t b = (uint32_t)((segmentsW + 1)*j + i - 1);
            uint32_t c = (uint32_t)((segmentsW + 1)*(j - 1) + i - 1);
            uint32_t d = (uint32_t)((segmentsW + 1)*(j - 1) + i);

            // Note OBJ counts from 1, so increment all indices by one
            a++; b++; c++; d++;

            if (j == (segmentsH - minj)) {
                // Triangle
                fprintf(fout, "f %d/%d/%d %d/%d/%d %d/%d/%d\n",
                        a,a,a,
                        c,c,c,
                        d,d,d);
            } else if (j == (1 - minj)) {
                // Triangle
                fprintf(fout, "f %d/%d/%d %d/%d/%d %d/%d/%d\n",
                        a,a,a,
                        b,b,b,
                        c,c,c);
                num_indices += 3;

            } else {
                // Quad - aka two triangles
                fprintf(fout, "f %d/%d/%d %d/%d/%d %d/%d/%d\n",
                        a,a,a,
                        b,b,b,
                        c,c,c);

                fprintf(fout, "f %d/%d/%d %d/%d/%d %d/%d/%d\n",
                        a,a,a,
                        c,c,c,
                        d,d,d);
            }
        }
    }
    fprintf(fout, "# Done!\n");


    fclose(fout);
    return (1);
}


int main(int argc, char *argv[])
{

    if (emit_sphere(parse_sphere(argc, argv))) return 0;
    //else if (emit_foo(parse_foo(argc, argv))) return 0;
    return -1;
}

