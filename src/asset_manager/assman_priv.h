#ifndef __ASSMAN_PRIV_H__
#define __ASSMAN_PRIV_H__


typedef struct assetOverlay {
    char *name;
    void *data;
    size_t size;
    struct assetOverlay *next;
} assetOverlay_t;

struct asset_magager {
    char *assetbase;
    int cwd; /** Inistal working dir of app */
    int kq;
    struct {
        asset_t* head;
        asset_t* tail;
        int count;
        int _pad0;
    } assets;

    struct {
        assetOverlay_t *head;
        int count;
        int _pad1;
    } overlays;
};

assetOverlay_t *findOverlay(assman_t *am, char *name);

#endif
