#include <assert.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#include <assets.h>
#include <util.h>
#include <dmalloc/dmalloc.h>

#include "assman_priv.h"


static void
do_assman_remove(assman_t *am, asset_t *asset)
{
    assert(am);
    assert(asset);
    assert(am->assets.head);
    if (am->assets.head == asset) {
        am->assets.head = asset->next;
        if (am->assets.tail == asset) {
            assert(am->assets.head == NULL);
            am->assets.tail = NULL;
        }
        am->assets.count--;
        return;
    }

    asset_t *a;
    for(a = am->assets.head; a; a=a->next) {
        if (a->next == asset) {
            a->next = asset->next;
            if (a->next == NULL) {
                assert(am->assets.tail == asset);
                am->assets.tail = a;
            }
            am->assets.count--;
            return;
        }
    }
}

int
asset_free(asset_t *asset)
{
    assman_t *am = asset->manager;
    assert(am);
    assert(asset);
    assert(asset->use > 0);

    asset->use--;
    if (asset->use > 0) {
        return asset->use;
    }

    if (asset->release) asset->release(asset);

    do_assman_remove(am, asset);
    dfree(asset->name);

    dfree(asset);

    return 0;
}

void
assman_add(assman_t *am, asset_t *asset)
{
    assert(am);
    assert(asset);

    if (am->assets.head == NULL) {
        am->assets.head = asset;
    } else {
        am->assets.tail->next = asset;
    }
    am->assets.tail = asset;
    asset->next = NULL;
    am->assets.count++;
    asset->use++;
    asset->manager = am;
}


assman_t *
assman_init(int argc UNUSED, char *argv[] UNUSED)
{
    assman_t *ret = dmalloc(sizeof(*ret));
    ret->assetbase = dstrdup("data");
    ret->cwd = open(".", O_RDONLY);
    return ret;
}

void
assman_destroy(assman_t *ass)
{
    assert(ass);

    asset_t *a, *next;
    for (a=ass->assets.head; a; a=next) {
        next = a->next;
        printf("WARNING: Potential leaky asset: %s[%d]\n", a->name, a->use);
        if (a->name) dfree(a->name);
        dfree(a);
    }

    assert(ass->assetbase);
    dfree(ass->assetbase);

    dfree(ass);
}

static int
is_match(asset_t *ass, char *name, asset_type_t type)
{
    if (strcmp(name, ass->name) != 0) return 0;
    return (type == ASS_ANY) || (type == ass->type);
}

asset_t *
assman_lookup(assman_t *ass, char *name, asset_type_t type)
{
    assert(ass);
    assert(name);

    asset_t *a;
    for (a=ass->assets.head; a; a=a->next) {
        if (is_match(a, name, type)) {
            a->use++;
            return a;
        }
    }
    return NULL;
}


FILE *
assman_write_file(assman_t *ass, char *out_path)
{
    char *fullpath;
    asprintf(&fullpath, "%s/%s", ass->assetbase, out_path);
    FILE *ret = fopen(fullpath, "w");
    if (!ret) {
        perror("fopen");
        printf("Cannot open %s", fullpath);
    }
    free(fullpath);
    return ret;
}

void
assman_debug_list(assman_t *ass)
{
    assert(ass);

    asset_t *a;
    if (ass->assets.count)
        printf("ASSETS: %d\n", ass->assets.count);
    int i = 1;
    for (a=ass->assets.head; a; a=a->next) {
        printf("ASSET %d: %p:%s. Type[%d]  next]%p]\n",
                i++, a, a->name, a->type, a->next);
    }
}
