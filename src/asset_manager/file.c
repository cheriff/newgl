#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <assets.h>
#include <util.h>
#include <dmalloc/dmalloc.h>

#include "assman_priv.h"

static file_asset_t*
lookup_file(assman_t *am, char *name)
{
    assert(am);
    assert(name);
    asset_t *ret = assman_lookup(am, name, ASS_FILE);
    if (ret) {
        return ret->asset_data;
    }
    return NULL;
}

static void
_release(asset_t *a)
{
    assert(a);
    file_asset_t *f = a->asset_data;
    util_munmap(f->file_data, f->size, f->fd);
}

file_asset_t *
assman_openFile(assman_t *am, char *name)
{
    assert(am);
    assert(name);

    /* IF we already have 'name', just return the asset */
    file_asset_t *ret = lookup_file(am, name);
    if (ret) {
        return ret;
    }

    int r;
    ret = dmalloc(sizeof(*ret));

    /* Check for overlays */
    assetOverlay_t *overlay = findOverlay(am, name);
    if (overlay) {
        printf("FOUND!\n");
        ret->file_data = overlay->data;
        ret->size = overlay->size;
        ret->fd = 0;
    } else {
    /* Otherwise find, load and return int */
        r = chdir(am->assetbase);
        if (r != 0) {
            printf("Cannot find asset base '%s' : %s\n", 
                    am->assetbase,
                    strerror(errno));
            dfree(ret);
            goto out;
        }

        ret->file_data = util_mmap(name, &ret->size, &ret->fd);
        if (!ret->file_data) {
            dfree(ret);
            ret = NULL;
            goto out;
        }
    }

    ret->asset.name = dstrdup(name);
    ret->asset.type = ASS_FILE;
    ret->asset.asset_data = ret;
    ret->asset.release = _release;

    assman_add(am, &ret->asset);
out:
    r = fchdir(am->cwd);
    if (r!=0) printf("Cannot return to homedir: %s\n",
            strerror(errno));

    return ret;
}

void
assman_closeFile(file_asset_t *file)
{
    assert(file);
    asset_free(&file->asset);
}
