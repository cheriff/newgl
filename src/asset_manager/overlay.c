#include <assert.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

#include <assets.h>
#include <util.h>
#include <dmalloc/dmalloc.h>

#include "assman_priv.h"

void *
assman_addOverlay(assman_t *am, char *name, void *data, size_t sz)
{
    assert(am != NULL);
    assert(name != NULL);

    assetOverlay_t *ov = dmalloc(sizeof(*ov));
    ov->name = dstrdup(name);
    ov->size = sz;
    ov->data = dmalloc(sz);
    if (data) {
        memcpy(ov->data, data, sz);
    }

    ov->next = am->overlays.head;
    am->overlays.head = ov;
    am->overlays.count++;

    return ov->data;
}

assetOverlay_t *
findOverlay(assman_t *am, char *name)
{
    assert(am);
    assert(name);
    assetOverlay_t *ov;
    for (ov = am->overlays.head; ov; ov=ov->next) {
        if (strcmp(name, ov->name) == 0) {
            return ov;
        }
    }
    return NULL;
}


