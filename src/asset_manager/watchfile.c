#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <assets.h>
#include <util.h>
#include <dmalloc/dmalloc.h>

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include "assman_priv.h"

file_change_func
assman_setFileOnchange(file_asset_t *file,
        file_change_func change)
{
    assert(file);
    file_change_func ret = file->on_change;
    file->on_change = change;

    if (change == NULL) {
        return ret;
    }

    assman_t *am = file->asset.manager;
    assert(am);
    if (am->kq == 0) {
        am->kq = kqueue();
    }

    struct kevent kev;
    EV_SET( &kev, file->fd,
            EVFILT_VNODE,
            EV_ADD | EV_ENABLE | EV_CLEAR,
            NOTE_DELETE|NOTE_WRITE|NOTE_EXTEND,
            0,
            file);

    int result = kevent(am->kq,
            &kev, 1, // Changes
            NULL, 0, // Read results
            NULL);   // Timeout
    if (result != 0) {
        perror("kevent");
        assert(!"failed");
    }

    return ret;
}

static void
handle_filechange(struct kevent *kev)
{
    file_asset_t *fa = kev->udata;
    assman_t *am = fa->asset.manager;

    struct {
        void *addr;
        size_t size;
        int fd;
        int _pad;
    } old = {
        fa->file_data,
        fa->size,
        fa->fd,
        0
    };


    chdir(am->assetbase);
    fa->file_data = util_mmap(fa->asset.name,
            &fa->size, &fa->fd);
    assert(fa->file_data);
    fchdir(am->cwd);

    struct kevent new_ev[2];
    EV_SET( &new_ev[0], old.fd,
            EVFILT_VNODE,
            EV_DELETE,
            0,
            0,
            NULL);

    EV_SET( &new_ev[1], fa->fd,
            EVFILT_VNODE,
            EV_ADD | EV_ENABLE | EV_CLEAR,
            NOTE_DELETE|NOTE_WRITE|NOTE_EXTEND,
            0,
            fa);


    int ret = kevent(am->kq, new_ev, 2, NULL, 0, NULL);
    assert(ret == 0);

    if (fa->on_change) fa->on_change(fa);
}

int
assman_update(assman_t *am)
{
    assert(am);
    if (am->kq == 0) {
        return 0;
    }

    int count = 0;

    struct timespec timeout = {0,0};
    struct kevent kev;

    int ret;
    while ((ret = kevent(am->kq, NULL, 0, &kev, 1, &timeout)) != 0) {
        handle_filechange(&kev);
        count++;
    }

    return count;
}
