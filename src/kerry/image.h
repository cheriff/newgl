#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <stdlib.h>
#include <stdio.h>

#include <texb.h>

int texb_write(void *data, int width, int height, int depth, FILE *filename);

#endif
