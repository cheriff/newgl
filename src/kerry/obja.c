#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "obja.h"
#include "vert_cache.h"

#include <dmalloc/dmalloc.h>
#include <util.h>


static char *
eatSpaces(char *c)
{
    while((*c==' ') || (*c == '\t')) c++;
    return c;
}


static int
isnewline(char c)
{
    return (c == '\r' || c == '\n');
}


static char *
process_comment(model_a_t *m, char *c)
{
    (void)m;
    while(!isnewline(*c)) c++;
    return c;
}

static char *
process_face(model_a_t *model, char *c)
{
    face_t *f = model->groups.active->faces +
                model->groups.active->num_faces;
    model->groups.active->num_faces++;
    model->num_faces++;

    assert(*c == 'f');
    c++;
    assert(isspace(*c));
    c = eatSpaces(c);

    unsigned int i;
    for(i = 0; i<MAX_FACE_VERTS; i++)
    {
        // Precondition: 'c' on first number of vert idx

        f->normal_idx[i] = 0;
        f->texcoord_idx[i] = 0;
        f->vert_idx[i] = 0;

        f->vert_idx[i] = (int)strtol(c, &c, 0);
        assert(f->vert_idx[i] != 0); //obj files are 1 based (no zeroes)
        if (f->vert_idx[i] < 0) {
            f->vert_idx[i] += model->num_vertices;
        } else {
            f->vert_idx[i]--; // But we want them zero-based for array indexing
        }

        if (isnewline(*c)) break;

        if (*c++ == '/') {
            assert(*c=='/' || *c=='-' || isdigit(*c));

            if (*c=='-' || isdigit(*c)) {
                f->texcoord_idx[i] = (int)strtol(c, &c, 0);
                assert(f->texcoord_idx[i] != 0);
                if (f->texcoord_idx[i] < 0) {
                    f->texcoord_idx[i] += model->num_texcoords;
                } else {
                    f->texcoord_idx[i]--;
                }

                // leave cursor on / of next, or nothing.
                assert(*c == '/' || isspace(*c));
            }

            assert(*c == '/' || isspace(*c));

            if (*c++ == '/') {
                f->normal_idx[i] = (int)strtol(c, &c, 0);
                assert(f->normal_idx[i] != 0);
                if (f->normal_idx[i] < 0) {
                    f->normal_idx[i] += model->num_normals;
                } else {
                    f->normal_idx[i]--;
                }

                // leave cursor on trailing spaces, or newline
                assert(isspace(*c));
            }
        }

        while (*c == ' ' || *c == '\t') {
            c++;
        }
        if (*c=='-' || isdigit(*c)) continue;
        if (isnewline(*c)) break;
    }
    f->num_verts = i+1;
    unsigned int num_tris = f->num_verts-2;

    model->groups.active->num_tris += num_tris;
    model->num_tris += num_tris;

    if (f->num_verts == MAX_FACE_VERTS+1) {
        return NULL;
    }

    return process_comment(model, c);
}

static char *
process_vertex(model_a_t *m, char *c)
{
    assert(m);
    assert(*c == 'v');
    c++;

    char *end = c;
    position_t *v = m->vertices + m->num_vertices;

    v->x = strtof(c, &end);
    assert(c != end);

    c = end;
    v->y = strtof(c, &end);
    assert(c != end);

    c = end;
    v->z = strtof(c, &end);
    assert(c != end);

    m->num_vertices++;
    return process_comment(m, c);
}

static char *
process_normal(model_a_t *m, char *c)
{
    assert(m);
    normal_t *n = m->normals + m->num_normals;

    assert(*c == 'v');
    c++;
    assert(*c == 'n');
    c++;

    char *end = c;

    n->x = strtof(c, &end);
    assert(c != end);

    c = end;
    n->y = strtof(c, &end);
    assert(c != end);

    c = end;
    n->z = strtof(c, &end);
    assert(c != end);

    m->num_normals++;
    return process_comment(m, c);
}

static char *
process_texcoord(model_a_t *m, char *c)
{
    assert(m);
    texcoord_t *t = m->texcoords + m->num_texcoords;


    assert(*c == 'v');
    c++;
    assert(*c == 't');
    c++;

    char *end = c;

    t->x = strtof(c, &end);
    assert(c != end);

    c = end;
    t->y = strtof(c, &end);
    assert(c != end);

    m->num_texcoords++;
    return process_comment(m, c);
}

static char *
process_v(model_a_t *m, char *c)
{
    switch(c[1]) {
        case ' ': return process_vertex(m, c);
        case 'n': return process_normal(m, c);
        case 't': return process_texcoord(m, c);
        default:
            printf("Unknown line: '");
            while(!isnewline(*c)) printf("%c", *c++);
            printf("'\n");
    }
    // just eat to end of line for now
    return process_comment(m, c);
}

static void
model_appendGroup(model_a_t *m, group_a_t *g)
{
    assert(m);
    assert(g);

    g->next = NULL;
    if (m->groups.head == NULL) {
        assert(m->groups.tail == NULL);
        assert(m->groups.count == 0);
        m->groups.head = g;
    } else {
        m->groups.tail->next = g;
    }
    m->groups.tail = g;
    m->groups.count++;
}

static char *
process_group(model_a_t *m, char *c)
{
    char *groupname = c = c+2;
    while (!isspace(*c)) {
        c++;
    }
    char *end = c;

    size_t len = (size_t)(end - groupname);

    group_a_t *g = dmalloc(sizeof(*g));
    g->name = dmalloc(len+1);
    memcpy(g->name, groupname, len);
    g->name[len] = 0;
    m->num_groups++;

    model_appendGroup(m, g);

    return end;
}

static char *
reProcess_group(model_a_t *m, char *c)
{
    group_a_t *match;

    if (m->groups.active) match = m->groups.active->next;
    else  match = m->groups.head;
    assert(match);


    char *groupname = c = c+2;
    assert(strncmp(match->name, groupname, strlen(match->name)) == 0);

    extern int verbose;
    if (verbose) {
        printf("\033[2KLoading: %d/%d %s\r", m->groups.count, m->num_groups, match->name);
        fflush(stdout);
    }

    m->groups.active = match;
    return process_comment(m, c);
}

static void
model_count(model_a_t *model, char *data, char *end)
{
    assert(model);
    assert(data);
    assert(end);

    int line = 0;
    for (char *c = data; c < end; c++) {
        line++;

        switch(*c) {
            case ' ':
            case '\t':
            case '\n':
            case '\r':
                continue;
            case 'u':
                // Ignore usemtl; fall-thru to comment
            case 'm':
                // Ignore mtlib; fall-thru to comment
            case 's':
                // Ignore smoothing groups; fall-thru to comment
            case '#':
                c = process_comment(model, c);
                assert(c);
                break;
            case 'g':
                c = process_group(model, c);
                assert(c);
                break;
            case 'f':
                model->groups.tail->num_faces++;
                c = process_comment(model, c); // eat and ignore remainder of line
                break;
            case 'v':
                switch(*(c+1)) {
                    case ' ': case '\t':
                        model->num_vertices++;
                        break;
                    case 't':
                        model->num_texcoords++;
                        break;
                    case 'n':
                        model->num_normals++;
                        break;
                    default:
                        printf("Unknown v-char: %c\n", c[1]);
                        assert(!"FAIL");
                }
                c = process_comment(model, c); // eat and ignore remainder of line
                break;
            default:
                printf("Unknown line-char: %c at line %d %ld\n", c[0],line,  c-data);
                assert(!"FAIL");
        }
    }
}

static void
model_alloc(model_a_t *model)
{
    assert(model);

    model->vertices = dmalloc(model->num_vertices * sizeof(position_t));
    model->num_vertices = 0;

    model->normals = dmalloc(model->num_normals * sizeof(normal_t));
    model->num_normals = 0;

    model->texcoords = dmalloc(model->num_texcoords * sizeof(texcoord_t));
    model->num_texcoords = 0;

    group_a_t *g;
    for (g = model->groups.head; g != NULL; g=g->next) {
        g->faces = dmalloc(g->num_faces * sizeof(face_t));
        g->num_faces = 0;
    }
    model->groups.count = 0;
}


static inline void
cache_index(model_a_t *model, face_t *face, unsigned int index)
{
    vertcache_item_t item;

    item.pos.x = model->vertices[face->vert_idx[index]].x;
    item.pos.y = model->vertices[face->vert_idx[index]].y;
    item.pos.z = model->vertices[face->vert_idx[index]].z;
    item.pos.w = 1.0f;

    item.normal.x = model->normals[face->normal_idx[index]].x;
    item.normal.y = model->normals[face->normal_idx[index]].y;
    item.normal.z = model->normals[face->normal_idx[index]].z;
    item.normal.w = 0.0f;

    item.tex.s = model->texcoords[face->texcoord_idx[index]].x;
    item.tex.t = 1-model->texcoords[face->texcoord_idx[index]].y;

    vertCache_insert(model->cache, &item);
}


static void
model_triangulate(model_a_t *model)
{
    assert(model);

    model->cache = vertCache_create(model->num_tris*3);

    group_a_t *g;
    for (g = model->groups.head; g != NULL; g=g->next) {
        unsigned int num_tris = g->num_tris;
        unsigned int num_verts = num_tris * 3;
        g->num_verts = num_verts;
        g->base_index = model->cache->num_indices;

        unsigned int i, f;
        for (i=0,f=0; f < g->num_faces; f++) {
            face_t *face = g->faces+f;

            unsigned int v;
            for (v=0; v < face->num_verts-2; v++) {
                cache_index(model, face, 0);
                cache_index(model, face, v+1);
                cache_index(model, face, v+2);

                g->num_indices += 3;

            }
        }
    }

}


void
obja_release(model_a_t* m)
{
    assert(m);
    dfree(m->vertices);
    dfree(m->normals);
    dfree(m->texcoords);
    vertCache_destroy(m->cache);

    group_a_t *g, *next;;
    for (g = m->groups.head; g != NULL; g=next) {
        next = g->next;
        dfree(g->faces);
        dfree(g->name);
        dfree(g);
    }
    dfree(m);
}

model_a_t *
obja_load(char *data, size_t size)
{
    assert(data);
    assert(size);
    char *end = data + size;

    model_a_t *model = dmalloc(sizeof(*model));

    model_count(model, data, end);
    model_alloc(model);


    int line = 0;
    for (char *c = data; c < end; c++) {
        if (c-1 == NULL) {
            printf("Error at line %d\n", line);
            assert(!"DIE");
        }
        line++;
        switch(*c) {
            case ' ':
            case '\t':
            case '\n':
            case '\r':
                continue;
            case 'u':
                // Ignore usemtl; fall-thru to comment
            case 'm':
                // Ignore mtlib; fall-thru to comment
            case 's':
                // Smoothing groups ignored
            case '#':
                c = process_comment(model, c);
                assert(c);
                break;
            case 'g':
                model->groups.count++;
                c = reProcess_group(model, c);
                assert(c);
                break;

            // FIXME
            case 'v':
                c = process_v(model, c);
                break;
            case 'f':
                c = process_face(model, c);
                break;
            case 'X':
                printf("EOF REACHED: size: %zu\n", size);
            default:
                printf("Unknown char: %c\n", *c);
                c = process_comment(model, c);
        }
    }

    model_triangulate(model);

    return model;
}
