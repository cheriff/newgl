#include "normalise.h"

#include <stdio.h>
#include <math.h>


int
normalise(model_a_t *m, float target)
{
    float minx, miny, minz;
    minx = miny = minz = HUGE_VALF;

    float maxx, maxy, maxz;
    maxx = maxy = maxz = -HUGE_VALF;

    position_t *p = m->vertices;
    for(unsigned int i=0; i<m->num_vertices; i++, p++) {
        if (p->x > maxx) maxx = p->x;
        if (p->x < minx) minx = p->x;

        if (p->y > maxy) maxy = p->y;
        if (p->y < miny) miny = p->y;

        if (p->z > maxz) maxz = p->z;
        if (p->z < minz) minz = p->z;
    }

    float midx = (maxx + minx) / 2.0f;
    float midy = (maxy + miny) / 2.0f;
    float midz = (maxz + minz) / 2.0f;

    float scalex = (maxx - minx) / target;
    float scaley = (maxy - miny) / target;
    float scalez = (maxz - minz) / target;

    float scale = fmaxf(fmaxf(scalex, scaley), scalez);

    p = m->vertices;
    for(unsigned int i=0; i<m->num_vertices; i++, p++) {
        p->x =  ((p->x - midx) / scale);
        p->y =  ((p->y - midy) / scale);
        p->z =  ((p->z - midz) / scale);
    }

    return 0;
}

int
add_normals(model_a_t *m, int smooth)
{
    if (m->num_normals) {
        return 0;
    }
//FIXME    printf("Adding normals not implemented\n");
    return smooth;
}
