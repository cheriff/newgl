#include "mata.h"

#include <matb.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>

void
matb_write(material_at *m, FILE *fp)
{
    assert(fp);
    assert(m);

    matb_header_t header;
    header.matb_magic = MATB_MAGIC;
    header.matb_version = MATB_CURRENT_VERSION;

    stpncpy(header.shader, m->shader, MATB_NAME_SZ);

    header.num_uniforms = m->uniforms.count;
    header.uniforms_offset = sizeof(header);

    assert(ftell(fp) == 0);
    size_t w = fwrite(&header, sizeof(header),1,fp);
    assert(w==1);

    assert(ftell(fp) == header.uniforms_offset);

    uniform_at *u;
    matb_uniform_t uniform;
    for (u= m->uniforms.head; u!=NULL; u=u->next) {

        stpncpy(uniform.name, u->name, MATB_NAME_SZ);
        uniform.type = u->type;
        uniform.size = u->size;

        assert(sizeof(u->data) == sizeof(float) * 16);
        memcpy(uniform.data, &u->data, sizeof(u->data));

        w = fwrite(&uniform, sizeof(uniform),1,fp);
        assert(w==1);
    }



}
