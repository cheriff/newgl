#ifndef __VERTCACHE_H__
#define __VERTCACHE_H__

typedef struct vertex_cache vertcache_t;

typedef struct vertcache_item{
    //public
    struct {
        float x,y,z,w;
    } pos;
    struct {
        float x,y,z,w;
    } normal;
    struct {
        float s, t;
    } tex;

    // private
    unsigned int index;
    unsigned int _pad0;
    struct vertcache_item *next;
} vertcache_item_t;

struct vertex_cache {
    unsigned int hash_size; // length of hashtable
    unsigned int num_items; // current inserted items

    unsigned int hits;      // hit and mis count
    unsigned int misses;    // hit and mis count

    unsigned int chain_steps; // number of steps through a chain
    unsigned int chain_entries; // times we didnt use the 0th entry of a chain

    unsigned int max_items;  // max possible inserted items
    unsigned int num_indices; // current position in indices

    vertcache_item_t **hashtable; // hash table: hash_size * pointers to item
    unsigned int *indices;        // indexing into items, max_items * integers
    vertcache_item_t *items;      // Backing store for items: max_items items.
                                  // These will end up being in order by index
                                  // And hopefully have < max_items actually
                                  // used

};

vertcache_t *vertCache_create(unsigned int max_entries);
void vertCache_destroy(vertcache_t *vt);

void vertCache_insert(vertcache_t *vt, vertcache_item_t *item);

#endif
