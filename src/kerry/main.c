#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <util.h>
#include <dmalloc/dmalloc.h>
#include "obja.h"
#include "mata.h"
#include "normalise.h"
#include "stb_image.h"
#include "image.h"

// TODO: some way to change this
extern int verbose;
int verbose = 0;

enum input_types {
    OBJA,
    IMG,
    MATA,
};
static char *
in2str(enum input_types e) {
    switch(e) {
        case OBJA: return "object";
        case IMG: return "image";
        case MATA: return "material";
    }
    return "UNKNOWN";
}

enum output_types  {
    OBJB,
    TEXB,
    MATB,
};

static char *
out2str(enum output_types e) {
    switch(e) {
        case OBJB: return "object";
        case TEXB: return "texture";
        case MATB: return "material";
    }
    return "UNKNOWN";
}


struct args {
    char * in_path;
    char * out_path;
    enum input_types input_type;
    enum output_types output_type;
};


static int
usage(char *name)
{
    printf("Usage: %s <infile> <outfile>\n", name);
    printf("Where:\n");
    printf("\t<infile> : Input file, one of the following types:\n");
    printf("\t\t.obja model file\n");
    printf("\t\t.jpg image file\n");
    printf("\t\t.png image file\n");
    printf("\t\t.tga image file\n");
    printf("\t<outfile>: Output file one of following types\n");
    printf("\t\t.objb binary model file\n");
    printf("\t\t.texb binary texture file\n");
    return 0;
}

static int
process_args(int argc, char *argv[], struct args *args_out)
{
    assert(argc>0);
    assert(argv!=NULL);
    assert(args_out);

    if (argc < 3) {
        return usage(argv[0]);
    }

    args_out->in_path = argv[1];
    args_out->out_path = argv[2];

    char *in_ext = strrchr(argv[1], '.');
    if (!in_ext) {
        in_ext = "<no file extension>";
        goto bad_in_file;
    }

    if (strcasecmp(in_ext, ".obja") == 0) {
        args_out->input_type = OBJA;
    } 
    else if (strcasecmp(in_ext, ".png") == 0) {
        args_out->input_type = IMG;
    }
    else if (strcasecmp(in_ext, ".tga") == 0) {
        args_out->input_type = IMG;
    }
    else if (strcasecmp(in_ext, ".jpg") == 0) {
        args_out->input_type = IMG;
    }
    else if (strcasecmp(in_ext, ".mata") == 0) {
        args_out->input_type = MATA;
    }
    else {
        bad_in_file:
        printf("Unknown input file type: '%s'\n", in_ext);
        return usage(argv[0]);
    }

    char *out_ext = strrchr(argv[2], '.');
    if (!out_ext) {
        out_ext = "<no file extension>";
        goto bad_out_file;
    }

    if (strcasecmp(out_ext, ".objb") == 0) {
        args_out->output_type = OBJB;
    } 
    else if (strcasecmp(out_ext, ".texb") == 0) {
        args_out->output_type = TEXB;
    } else if (strcasecmp(out_ext, ".matb") == 0) {
        args_out->output_type = MATB;
    }
    else {
        bad_out_file:
        printf("Unknown output file type: '%s'\n", out_ext);
        return usage(argv[0]);
    }

    if ( (int)args_out->output_type != (int)args_out->input_type) {
        printf("Illegal file conversion: %s (%s) --> %s (%s)\n",
                in_ext, in2str(args_out->input_type),
                out_ext, out2str(args_out->output_type));
        return usage(argv[0]);
    }

    return 1;
}

int
main(int argc, char *argv[])
{
    int ret = 0;
    struct args args;

    if (!process_args(argc, argv, &args)) {
        ret =-1;
        goto fail;
    }

    size_t size;
    void *data = util_mmap(args.in_path, &size, NULL);

    if (!data) {
        ret =-1;
        goto fail;
    }


    if (verbose > 0)
        printf("Will Convert %s -> %s\n",
                args.in_path,
                args.out_path);

    if (args.input_type == OBJA) {
        model_a_t *m = obja_load(data, size);
        if (!m) {
            printf("Parsing %s failed\n", args.in_path);
            goto fail;
        }

        normalise(m, 100.0f);
        add_normals(m, 1);

        FILE *fout = fopen(args.out_path, "wb");
        if (!fout) {
            perror("fopen");
        } else {
            objb_write(m, fout);
        }

        obja_release(m);
    }
    else if (args.input_type == IMG) {
        unsigned char * pixels;
        int x, y, comp;
        pixels = stbi_load_from_memory(data, (int)size, &x, &y, &comp, 4);
        if (!pixels) printf("FAILED: %s\n", stbi_failure_reason());

        FILE *fout = fopen(args.out_path, "wb");
        if (!fout) {
            perror("fopen");
        } else {
            texb_write(pixels, x, y, 4, fout);
            dfree(pixels);
        }
    }
    else if (args.input_type == MATA) {
        material_at *mat = mata_load(data, size);
        if (!mat) {
            printf("Parsing %s failed\n", args.in_path);
            goto fail;
        }

        FILE *fout = fopen(args.out_path, "wb");
        if (!fout) {
            perror("fopen");
        } else {
            matb_write(mat, fout);
        }

        mata_release(mat);


    }
    else assert(!"Bad imput type");

    if (verbose > 1)
        dmalloc_analyze();

    return 0;

fail:
    if (verbose > 1)
        dmalloc_analyze();

    return -1;
}
