#ifndef __MATA_H__
#define __MATA_H__

#include <material.h>



typedef struct uniform_a {
    uint32_t type;
    uint32_t size;
    union {
        float f[16];
        int i[4];
        short s[4];
        double d;
        char c[64];
    } data;
    char *name;
    struct uniform_a *next;
} uniform_at;


typedef struct {
    char *shader;
    struct {
        uniform_at *head;
        uniform_at *tail;
        unsigned int count;
        unsigned int _pad0;
    } uniforms;
} material_at;

material_at *mata_load(char *data, size_t size);
void mata_release(material_at *mat);

void matb_write( material_at *mat, FILE *fout);

#endif
