#include <assert.h>

#include "image.h"


int
texb_write(void *data, int width, int height, int depth, FILE *fp)
{
    assert(data);
    assert(fp);

    assert(width < 0xFFFF);
    assert(height < 0xFFFF);
    assert(depth <= 4);

    texb_header_t header = {
        .texb_magic   = TEXB_MAGIC,
        .texb_version = TEXB_CURRENT_VERSION,
        .width        = (uint16_t)width,
        .height       = (uint16_t)height,
        .depth        = (uint16_t)depth,
        .pad          = 0,
    };

    size_t r = fwrite(&header, sizeof(header), 1, fp);
    assert(r==1);

    r = fwrite(data, (size_t)depth, (size_t)(width*height), fp);
    assert(r == (size_t)(width*height));
    return 0;
}

