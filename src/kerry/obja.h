#ifndef __OBJ_H__
#define __OBJ_H__

#include <stdlib.h>
#include <stdio.h>

#include <objb.h>

#include "vert_cache.h"

typedef struct vec4 position_t;
typedef struct vec4 normal_t;
typedef struct vec4 texcoord_t;

typedef struct face face_t;
typedef struct model_a model_a_t;
typedef struct group_a group_a_t;

struct model_a {
    vertcache_t *cache;
    position_t *vertices;
    normal_t *normals;
    texcoord_t *texcoords;
    struct {
        group_a_t *head;
        group_a_t *tail;
        group_a_t *active;
        unsigned int count;
        int pad0;
    } groups;

    unsigned int num_vertices;
    unsigned int num_normals;
    unsigned int num_texcoords;
    unsigned int num_groups;
    unsigned int num_faces;
    unsigned int num_tris;
};

struct group_a {
    char *name;
    face_t *faces;
    group_a_t *next;

    unsigned int base_index;
    unsigned int num_indices;

    unsigned int num_verts;
    unsigned int num_faces;

    unsigned int pad;
    unsigned int num_tris;
};

struct vec4 {
    float x, y, z, w;
};

#define MAX_FACE_VERTS 150
struct face {
    int vert_idx[MAX_FACE_VERTS];
    int texcoord_idx[MAX_FACE_VERTS];
    int normal_idx[MAX_FACE_VERTS];
    unsigned int num_verts;
};

model_a_t * obja_load(char *data, size_t size);
void objb_write(model_a_t *model, FILE *fp);
void obja_release(model_a_t* m);

#endif
