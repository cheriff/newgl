#include "mata.h"

#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include <dmalloc/dmalloc.h>

static int
isSpace(char c)
{
    return (c == '\t' || c == ' ');
}

static char *
eatSpaces(char *c)
{
    assert(isSpace(*c));
    while(isSpace(*c)) c++;
    return c;
}


static int
isnewline(char c)
{
    return (c == '\r' || c == '\n');
}


static char *
process_comment(char *c)
{
    while(!isnewline(*c)) c++;
    return c;
}

static size_t
word_len(char *c)
{
    char *start = c;
    while (!isspace(*c)) {
        c++;
    }
    char *end = c;

    size_t len = (size_t)(end - start);
    return len;
}

static char *
process_shader(material_at *mat, char *c)
{
    assert(mat);
    assert(c);
    assert(*c == 's');

    c = eatSpaces(c+1);
    size_t name_len = word_len(c);

    if (mat->shader != NULL) {
        printf("Error: Multiple shaders named\n");
        return NULL;
    }

    mat->shader = dmalloc(name_len + 1);
    memcpy(mat->shader, c, name_len);
    mat->shader[name_len] = 0;

    c+= name_len;
    return c;
}

static uniform_at*
add_uniform(material_at *mat)
{
    assert(mat);
    uniform_at *ret = dmalloc(sizeof(*ret));

    if (mat->uniforms.count == 0) {
        mat->uniforms.head = ret;
    } else {
        mat->uniforms.tail->next = ret;
    }

    ret->next = NULL;
    mat->uniforms.tail = ret;
    mat->uniforms.count++;
    return ret;
}

static char *
process_uniform_texture(material_at *mat, char *c)
{
    assert(mat);
    assert(c);
    assert(*c == 't');
    c++;

    uint32_t len;
    switch(*c) {
        case '1':
            len = 1;
            break;
        case '2':
            len = 2;
            break;
        case '3':
            len = 3;
            break;
        case '6':
            len = 6;
            break;
        default:
            printf("Bad texture length: '%c'\n", *c);
            return NULL;
    }
    c++;
    c = eatSpaces(c);

    uniform_at *uni = add_uniform(mat);
    uni->type = UT_TEXTURE;
    uni->size = len;

    size_t name_len = word_len(c);
    uni->name = dmalloc(name_len + 1);
    memcpy(uni->name, c, name_len);
    uni->name[name_len] = 0;
    c+= name_len;

    c = eatSpaces(c);

    name_len = word_len(c);
    assert(name_len < 63);
    stpncpy(uni->data.c, c, name_len);
    uni->data.c[63] = 0;
    c+= name_len;

    return c;
}

static char *
process_uniform_float(material_at *mat, char *c)
{
    assert(mat);
    assert(c);
    assert(*c == 'f');
    c++;

    uint32_t len;
    switch(*c) {
        case ' ':
        case '\t':
        case '1':
            len = 1;
            break;
        case '2':
            len = 2;
            break;
        case '3':
            len = 3;
            break;
        case '4':
            len = 4;
            break;
        default:
            printf("Bad float length: '%c'\n", *c);
            return NULL;
    }
    c++;
    c = eatSpaces(c);

    uniform_at *uni = add_uniform(mat);
    uni->type = UT_FLOAT;
    uni->size = len;

    size_t name_len = word_len(c);
    uni->name = dmalloc(name_len + 1);
    memcpy(uni->name, c, name_len);
    uni->name[name_len] = 0;
    c+= name_len;
    c = eatSpaces(c);

    char *end;
    for(unsigned int i=0; i<len; i++) {
        uni->data.f[i] = strtof(c, &end);
        assert(c != end);
        c = end;
    }


    return c;
}


static char *
process_uniform(material_at *mat, char *c)
{
    assert(mat);
    assert(c);
    assert(*c);

    c++;
    switch(*c) {
//      case 'b': return process_uniform_bool(mat, c);
//      case 's': return process_uniform_short(mat, c);
//      case 'i': return process_uniform_int(mat, c);
        case 'f': return process_uniform_float(mat, c);
//      case 'd': return process_uniform_double(mat, c);
        case 't': return process_uniform_texture(mat, c);
        default:
            printf("Unknown uniform type: '%c'\n", *c);
            return NULL;
    }


}

material_at *
mata_load(char *data, size_t size)
{
    assert(data);
    assert(size);
    char *end = data +size;

    material_at *ret = dmalloc(sizeof(*ret));

    int line = 0;
    for (char *c = data; c< end; c++) {
        line++;

        switch(*c) {
            case ' ':
            case '\t':
            case '\n':
            case '\r':
                continue;
            case '#':
                c = process_comment(c);
                assert(c);
                break;
            case 's':
                c = process_shader(ret, c);
                if (!c) return NULL;
                break;
            case 'u':
                c = process_uniform(ret, c);
                if (!c) return NULL;
                break;
            default:
                printf("Unexpected character: '%c'\n", *c);
                return NULL;
        }
    }

    return ret;
}

void
mata_release(material_at *m)
{
    assert(m);
    if (m->shader) dfree(m->shader);
    uniform_at *u, *next;
    for (u=m->uniforms.head; u; u=next) {
        next = u->next;
        if (u->name) dfree(u->name);
        dfree(u);
    }
    dfree(m);
}

