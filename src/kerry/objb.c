#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "obja.h"

#include <util.h>

void
objb_write(model_a_t *model, FILE *fp)
{
    assert(model);
    assert(fp);

    objb_header_t header;
    header.objb_magic = OBJB_MAGIC;
    header.objb_version = OBJB_CURRENT_VERSION;

    header.num_indices = model->cache->num_indices;
    header.indices_offset = sizeof(header);

    header.num_vertices = model->cache->num_items;
    header.vertices_offset = header.indices_offset + (header.num_indices * sizeof(uint32_t));

    header.num_groups = model->groups.count;
    header.groups_offset = header.vertices_offset + (header.num_vertices * sizeof(objb_vertex_t));


    assert(ftell(fp) == 0);
    size_t w = fwrite(&header, sizeof(header),1,fp);
    assert(w==1);


    assert(ftell(fp) == header.indices_offset);
    w = fwrite(model->cache->indices, sizeof(unsigned int), header.num_indices, fp);
    assert(w == header.num_indices);

    assert(ftell(fp) == header.vertices_offset);
    assert(sizeof(objb_vertex_t) == sizeof(float) * 10);
    for (unsigned int i=0; i < header.num_vertices; i++) {
        if (model->cache->items[i].pos.w != 1.0f) printf("POS NOT ONE\n");
        w = fwrite(&model->cache->items[i].pos, sizeof(float), 4, fp);
        assert(w == 4);

        if (model->cache->items[i].normal.w != 0.0f) printf("NORMAL NOT Zero\n");
        w = fwrite(&model->cache->items[i].normal, sizeof(float), 4, fp);
        assert(w == 4);

        w = fwrite(&model->cache->items[i].tex, sizeof(float), 2, fp);
        assert(w == 2);
    }

    objb_group_t group;
    group_a_t *g;
    assert(ftell(fp) == header.groups_offset);
    for(g=model->groups.head; g; g=g->next) {
        stpncpy(group.name, g->name, OBJB_NAME_SZ);
        group.index_base = g->base_index;
        group.num_indices = g->num_indices;
        w = fwrite(&group, sizeof(group), 1, fp);
        assert(w==1);
    }

    extern int verbose;
    if (verbose) {
        printf("Wrote %ld bytes\n", ftell(fp));
    }
    fclose(fp);
}
