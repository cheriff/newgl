#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <dmalloc/dmalloc.h>

#include "vert_cache.h"


#define NUM_BYTES 40

static const uint32_t PRIMES[] = {
    2,  11, 57, 101,
    89, 61, 31, 43,
    97, 23, 1,  47,
};

static unsigned int
hash_vertex(vertcache_item_t *item)
{
    uint32_t hash = 0;
    uint32_t *as_ints = (uint32_t*)(&item->pos);

    for (int i=0; i<(NUM_BYTES/4); i++) {
        hash += as_ints[i] * PRIMES[i];
    }
    return hash;
}


vertcache_t *
vertCache_create(unsigned int max_entries)
{

    double sizef = pow(2, ceil(log(max_entries*2)/log(2)));
    unsigned sz = (unsigned)sizef;
    assert( (sz & (sz-1)) == 0);
    vertcache_t *ret;

    ret = dmalloc(sizeof(*ret));

    ret->hashtable = dmalloc(sz * sizeof(vertcache_item_t*));
    ret->hash_size = sz;

    ret->items = dmalloc(max_entries * sizeof(vertcache_item_t));
    ret->indices = dmalloc(max_entries * sizeof(unsigned int));
    ret->max_items = max_entries;

    return ret;
}

extern int verbose;

void
vertCache_destroy(vertcache_t *vt)
{
    assert(vt);
    if (verbose) {
        printf("Cache: %d capacity, %d entries (%f %%)\n",
                vt->hash_size, vt->num_items,
                (vt->num_items*1.0f) / (vt->hash_size*1.0f) * 100.0f);
        printf("\t%d chain enties, for %d steps\n",
                vt->chain_entries,
                vt->chain_steps);
        printf("\t%d hits, for %d misses\n",
                vt->hits, vt->misses);
    }


    dfree(vt->hashtable);
    dfree(vt->items);
    dfree(vt->indices);
    dfree(vt);
}


void
vertCache_insert(vertcache_t *vt, vertcache_item_t *item)
{
    assert(vt);
    assert(item);
    unsigned int hash = hash_vertex(item) % vt->hash_size;
    assert(hash < vt->hash_size);

    unsigned int steps = 0;

    vertcache_item_t * bucket;
    for(bucket = vt->hashtable[hash]; bucket; bucket=bucket->next) {
        if (memcmp(bucket, item, NUM_BYTES) == 0) {
            vt->chain_steps += steps;
            if (steps) vt->chain_entries++;
            vt->hits++;
            goto done;
        }
        steps++;
    }

    bucket = vt->items + vt->num_items;
    bucket->pos = item->pos;
    bucket->normal = item->normal;
    bucket->tex = item->tex;
    bucket->index = vt->num_items;
    bucket->next = vt->hashtable[hash];

    vt->hashtable[hash] = bucket;
    vt->num_items++;
    vt->misses++;

    assert(vt->num_items <= vt->max_items);

done:
    vt->indices[vt->num_indices] = bucket->index;
    vt->num_indices++;
}
