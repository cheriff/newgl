#ifndef __SCENE_PRIV_H__
#define __SCENE_PRIV_H__

struct sceneNode {
    char *name;
    matrix_t matrix;

    sceneNode_t *parent;
    sceneNode_t *children;
    sceneNode_t *next;
    scene_t *scene;

    drawFunction_t func;
    void *user_data;

    unsigned int depth;
    int _pad0;
};

struct scene {
    sceneNode_t root;

    matrix_t projection;
    matrix_t view;
    matrix_t *matrix_stack;
    unsigned int current_depth;
    unsigned int matrix_stack_size;
};

#endif
