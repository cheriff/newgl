#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sys/mman.h>

#include <GLFW/glfw3.h>
#include <shader.h>
#include <util.h>
#include <dmalloc/dmalloc.h>

#include "scene_priv.h"
#include <matrix.h>

static shader_t *active_shader = NULL;



static void
_release(asset_t *a)
{
    assert(a);
    assert(a->type == ASS_SHADER_PROG);

    shader_t *shader = a->asset_data;
    assert(shader);

    glDeleteShader(shader->vs);
    glDeleteShader(shader->fs);
    glDeleteProgram(shader->program);

    assert(shader->vs_file_asset);
    assman_closeFile(shader->vs_file_asset);

    assert(shader->vs_file_asset);
    assman_closeFile(shader->fs_file_asset);
}


static unsigned int
shader_from_asset(file_asset_t *fa, GLenum shaderType)
{
    assert(fa);

    unsigned int shader = glCreateShader(shaderType);
    assert(shader != 0);

    const GLchar *sources[] = { (char*)fa->file_data };
    GLint sizes[] = { (GLint)fa->size };

    glShaderSource (shader, 1, sources, sizes);
    glCompileShader(shader);


    int compiled_ok;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled_ok);
    if (!compiled_ok)  {
        int log_len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);

        char *log = alloca(log_len);
        glGetShaderInfoLog(shader, log_len, NULL, log);
        printf("COMPILE FAIL: %s\n%s\n\n", fa->asset.name, log);
        glDeleteShader(shader);
        shader = 0;
    }

    return shader;
}

static unsigned int
program_from_shaders(unsigned int vertex_shader, unsigned int fragment_shader)
{
    assert(vertex_shader);
    assert(fragment_shader);
    assert(glGetError() == 0);

     unsigned int program = glCreateProgram ();
     glAttachShader (program, vertex_shader);
     glAttachShader (program, fragment_shader);

     glBindAttribLocation (program, 1, "vertex_position");

     glLinkProgram (program);

     int link_status;
     glGetProgramiv(program, GL_LINK_STATUS, &link_status);
     if (!link_status) {
         int log_len;
         glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_len);
         char *log = alloca(log_len);

         glGetProgramInfoLog(program, log_len, NULL, log);
         printf("Link FAIL:\n%s\n", log);
         glDeleteProgram(program);
         program = 0;
     }

     return program;
}

shader_t *
shader_lookup(assman_t *assets, char *name)
{
    asset_t *ret = assman_lookup(assets, name, ASS_SHADER_PROG);
    if (ret) return ret->asset_data;
    return NULL;
}

static void
shader_reload(file_asset_t *file)
{
    shader_t *shader = file->asset.link;

    unsigned int fs = 0;
    unsigned int vs = 0;
    unsigned int program = 0;

    fs = shader->fs;
    vs = shader->vs;
    assert(fs != 0);
    assert(vs != 0);


    if (file == shader->vs_file_asset) {
        printf("Re-compiling vertex shader:\n");
        vs = shader_from_asset(file, GL_VERTEX_SHADER);
        if (!vs) {
            printf("\nVertex Shader compilation failed, not reloading shader\n");
            goto done;
        } else printf("\tOK\n");
    } else if (file == shader->fs_file_asset) {
        printf("Re-compiling fragment shader\n");
        fs = shader_from_asset(file, GL_FRAGMENT_SHADER);
        if (!fs) {
            printf("\nFragment Shader compilation failed, not reloading shader\n");
            goto done;
        } else printf("\tOK\n");
    }
    // this should never happen, unless I've broken somehting
    else assert(!"Cannot reload non-linked shader");

    if ((fs==0) || (vs==0)) {
        printf("Compilation failed, not reloading shader\n");
        goto done;
    }

    printf("Re-linking shader program\n");
    program = program_from_shaders(vs, fs);
    if (!program) {
        printf("\tLinking failed, not reloading shader\n");
        goto done;
    } else printf("\tOK\n");

    // This could be racy .. not clear if this can crash, error, or
    // just show a transient graphical glitch
    shader->projection_location = glGetUniformLocation (program, "projection");
    shader->view_location = glGetUniformLocation (program, "view");

    /* Swap in the new values.
     * Take care that 'shader' never contains a deleted value
     */
    unsigned int old = shader->program;
    shader->program = program;
    program = old;

    old = shader->fs;
    shader->fs = fs;
    fs = old;

    old = shader->vs;
    shader->vs = vs;
    vs = old;

    // So far so good, fall through to free the old shader elements
    // By freeing old ones after creating new ones, we ensure the same
    // GL ID isnt re-used. Other code may detect the numerical value
    // of 'program' changing (i.e to rebind the new shader)

done:
        if (vs) glDeleteShader(vs);
        if (fs) glDeleteShader(fs);
        if (program) glDeleteProgram(program);
}

shader_t *
shader_load(assman_t *assets, char *name)
{
    assert(name);
    assert(assets);
    char *shader_name = NULL;
    file_asset_t *vs_source = NULL;
    file_asset_t *fs_source = NULL;

    shader_t *ret = shader_lookup(assets, name);
    if (ret) {
        return ret;
    }

    asprintf(&shader_name, "shaders/%s.vert.glsl", name);
    assert(shader_name);
    vs_source = assman_openFile(assets, shader_name);
    if (!vs_source) {
        printf("No VERTEX shader: %s\n", shader_name);
        goto fail;
    }

    sprintf(shader_name, "shaders/%s.frag.glsl", name);
    fs_source = assman_openFile(assets, shader_name);
    if (!fs_source) {
        printf("No FRAGMENT shader: %s\n", shader_name);
        goto fail;
    }

    // now that the files are found, begin creating stuff

    ret = dmalloc(sizeof(*ret));

    ret->fs = shader_from_asset(fs_source, GL_FRAGMENT_SHADER);
    if (!ret->fs) goto fail;
    ret->fs_file_asset = fs_source;

    ret->vs = shader_from_asset(vs_source, GL_VERTEX_SHADER);
    if (!ret->vs) goto fail;
    ret->vs_file_asset = vs_source;

    ret->program = program_from_shaders(ret->vs, ret->fs);
    if (!ret->program) goto fail;

    ret->asset.name = dstrdup(name);
    ret->asset.asset_data = ret;
    ret->asset.type = ASS_SHADER_PROG;
    ret->asset.release = _release;
    assert((void*)ret == (void*)&ret->asset);
    assman_add(assets, &ret->asset);

    const int print_attrs = 1;
    if (print_attrs) {

        int attributes;
        int uniforms;
        static char item_name[1024] = {0};
        unsigned int type;
        int size;

        glGetProgramiv (ret->program, GL_ACTIVE_ATTRIBUTES, &attributes);
        for (int i=0; i<attributes; i++) {
            printf("Attribute %d\n", i);
            glGetActiveAttrib(ret->program, (GLuint)i, 1024, NULL, &size, &type, item_name);
            printf("\tName: %s\n", item_name);
            printf("\tType: %x / %s\n", type, glType_to_string(type));
            printf("\tSize: %d\n", size);
            int location = glGetAttribLocation (ret->program, item_name);
            printf("\tLocation = %d\n", location);
        }

        glGetProgramiv (ret->program, GL_ACTIVE_UNIFORMS, &uniforms);
        for (int i=0; i<uniforms; i++) {
            printf("Uniform %d\n", i);
            glGetActiveUniform (ret->program, (GLuint)i, 1024, NULL, &size, &type, item_name);
            printf("\tName: %s\n", item_name);
            printf("\tType: %x / %s\n", type, glType_to_string(type));
            printf("\tSize: %d\n", size);
            int location = glGetUniformLocation (ret->program, item_name);
            printf("\tLocation = %d\n", location);
        }
    }

    free(shader_name);

    ret->projection_location = glGetUniformLocation (ret->program, "projection");
    ret->view_location = glGetUniformLocation (ret->program, "view");

    vs_source->asset.link = ret;
    fs_source->asset.link = ret;

    assman_setFileOnchange(vs_source, shader_reload);
    assman_setFileOnchange(fs_source, shader_reload);

    return ret;


fail:
    free(shader_name);
    if (vs_source) assman_closeFile(vs_source);
    if (fs_source) assman_closeFile(fs_source);
    if (ret) {
        if (ret->vs) glDeleteShader(ret->vs);
        if (ret->fs) glDeleteShader(ret->fs);
        if (ret->program) glDeleteProgram(ret->program);
        dfree(ret);
    }
    return NULL;
}

int
shader_uniform_info(shader_t *shader, uniform_info_t *info, char *name)
{
    assert(shader);
    assert(info);
    assert(name);

    const char *nameList[1] = { name };

    GLuint index;
    glGetUniformIndices(shader->program, 1,
            nameList, &index);

    if (index == GL_INVALID_INDEX) return -1;

    glGetActiveUniform(shader->program, index,
            0, NULL,
            &info->size, &info->type, NULL);

    info->location = glGetUniformLocation (shader->program, name);
    assert(info->location >= 0);

    return 0;
}

int
shader_activate(scene_t* scene, shader_t *shader)
{
    assert(glGetError() == 0);
    assert(shader);
    if ((shader != active_shader) || (shader->program != shader->old_program)) {
        active_shader = shader;
        shader->old_program = shader->program;
        glUseProgram (shader->program);
        glUniformMatrix4fv (shader->projection_location,
                1, GL_FALSE,
                scene->projection.f);
    }

    glUniformMatrix4fv (shader->view_location,
            1, GL_FALSE,
            (scene->matrix_stack+scene->current_depth)->f);

    return shader->view_location;
}


void
shader_destroy(shader_t * shader)
{
    assert(shader);
    asset_free(&shader->asset);
}
