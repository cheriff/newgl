#include <assert.h>
#include <stdlib.h>

#include <material.h>
#include <texture.h>
#include <matb.h>
#include <dmalloc/dmalloc.h>

static GLenum
glType_from_matb(uint32_t type, uint32_t size) 
{
    switch(type) {
        case UT_FLOAT:
            switch (size) {
                case 1: return GL_FLOAT;
                case 2: return GL_FLOAT_VEC2;
                case 3: return GL_FLOAT_VEC3;
                case 4: return GL_FLOAT_VEC4;
            }
            break;
        case UT_INT:
            switch (size) {
                case 1: return GL_INT;
                case 2: return GL_INT_VEC2;
                case 3: return GL_INT_VEC3;
                case 4: return GL_INT_VEC4;
            }
            break;
        case UT_TEXTURE:
            switch(size) {
                case 1: return GL_SAMPLER_1D;
                case 2: return GL_SAMPLER_2D;
                case 3: return GL_SAMPLER_3D;
                case 6: return GL_SAMPLER_CUBE;
            }
    }
    printf("Unknown type:size: %d:%d\n", type, size);
    assert(!"FAIL");
}

material_t *
material_lookup(assman_t *assets, char *name)
{
    asset_t *ret = assman_lookup(assets, name, ASS_MATERIAL);
    if (ret) return ret->asset_data;
    return NULL;
}

static int
material_from_asset(assman_t *assets, material_t *mat)
{
    assert(mat);
    void *data = mat->file_asset->file_data;
    size_t size = mat->file_asset->size;

    if (size < sizeof(matb_header_t)) {
        printf("FILE TO SMALL\n");
        return -1;
    }

    assert(((uintptr_t)data & 0xF) == 0);
    matb_header_t *header = (matb_header_t *)data;

    if (header->matb_magic != MATB_MAGIC) {
        printf("BAD MAGIC: Got %08x; expected %08x\n",
                header->matb_magic, MATB_MAGIC);
        return -1;
    }

    if (header->matb_version != MATB_CURRENT_VERSION) {
        printf("BAD VERSION: Got %d.%d; expected %d.%d\n",
                MATB_VERSION_MAJ(header->matb_version),
                MATB_VERSION_MIN(header->matb_version),
                MATB_VERSION_MAJ(MATB_CURRENT_VERSION),
                MATB_VERSION_MIN(MATB_CURRENT_VERSION));
        return -1;
    }

    size_t total_sz = sizeof(matb_header_t) + 
        header->num_uniforms * sizeof(matb_uniform_t);

    if (size < total_sz) {
        printf("FILE TOO SMALL:");
        printf("%d uniforms should be %ld, not %ld\n",
                header->num_uniforms, total_sz, size);
            return -1;
    }

    if (size > total_sz) {
        printf("WARNING: FILE TOO Large?");
        printf("%d uniforms should be %ld, not %ld\n",
                header->num_uniforms, total_sz, size);
    }


    mat->shader = shader_load(assets, header->shader);
    if (!mat->shader) {
        printf("ERROR: Referenced bad shader: '%s'\n",
                header->shader);
        return -1;
    }

    mat->uniforms = dmalloc(header->num_uniforms * sizeof(uniform_t));
    matb_uniform_t *u = (matb_uniform_t*)((uintptr_t)data + header->uniforms_offset);

    unsigned int o =0;
    for (unsigned int i=0; i<header->num_uniforms; i++, o++) {
        uniform_t *uniform = mat->uniforms + o;

        uniform->gl_type = glType_from_matb(u[i].type, u[i].size);
        uniform->matb_type = u[i].type;
        uniform->matb_size = u[i].size;

        uniform_info_t info;
        if (shader_uniform_info(mat->shader, &info, u[i].name) == -1) {
            printf("Warning: Uniform %s does not exist (or is inactive) in shader %s\n",
                    u[i].name, mat->shader->asset.name);
            // Forget this one ever existed
            o--;
            continue;
        }

        if (info.type != uniform->gl_type) {
            printf("Error: type in shader(%s) does not match type in material(%s)\n",
                    glType_to_string(info.type),
                    glType_to_string(uniform->gl_type));
            return -1;
        }

        if (u[i].type == UT_TEXTURE) {
            printf("texture type is %02x\n", uniform->gl_type);
            char *texture_name = (char*)u[i].data;
            uniform->data.texture = texture_load(assets, texture_name);
            if (!uniform->data.texture) {
                printf("Unable to open material texture: %s\n", texture_name);
                return -1;
            }
        } else {
            uniform->data.f = (float*)(void*)u[i].data;
        }


        uniform->location = info.location;
        uniform->name = dstrdup(u[i].name);
        mat->num_uniforms++;
    }

    return 0;
}

static void
_release(asset_t *a)
{
    assert(a);
    assert(a->type == ASS_MATERIAL);

    material_t *material = a->asset_data;
    assert(material);

    if (material->shader) shader_destroy(material->shader);

    if (material->uniforms) {
        for (unsigned int i=0; i<material->num_uniforms; i++) {
            uniform_t *uniform = material->uniforms + i;
            if (uniform->name) dfree(uniform->name);

            if (uniform->matb_type == UT_TEXTURE) {
                texture_destroy(uniform->data.texture);
            }
        }
        dfree(material->uniforms);
    }

    assert(material->file_asset);
    assman_closeFile(material->file_asset);
}

material_t *
material_load(assman_t *assets, char *name)
{
    assert(assets);
    assert(name);

    material_t *ret = material_lookup(assets, name);
    if (ret) {
        return ret;
    }

    char *material_name;
    asprintf(&material_name, "materials/%s.matb", name);
    assert(material_name);

    file_asset_t *material_file = NULL;
    material_file = assman_openFile(assets, material_name);
    if (!material_file) {
        printf("No Material: %s\n", material_name);
        goto fail;
    }

    ret = dmalloc(sizeof(*ret));
    ret->asset.name = dstrdup(name);
    ret->asset.asset_data = ret;
    ret->asset.type = ASS_MATERIAL;
    ret->asset.release = _release;
    ret->file_asset = material_file;
    assert((void*)ret == (void*)&ret->asset);

    if (material_from_asset(assets, ret)) {
        goto fail;
    }

    assman_add(assets, &ret->asset);
    free(material_name);
    return ret;

fail:
    free(material_name);
    if (material_file) assman_closeFile(material_file);
    if (ret) {
        if (ret->asset.name) dfree(ret->asset.name);
    }
    return NULL;
}

void
material_activate(scene_t *scene, material_t *mat)
{
    assert(mat);
    assert(scene);

    shader_activate(scene, mat->shader);

    unsigned int texture_unit = 0;
    unsigned int i;
    for (i=0; i<mat->num_uniforms; i++) {
        switch(mat->uniforms[i].gl_type) {
            case GL_FLOAT_VEC2:
                glUniform2fv(mat->uniforms[i].location, 1,
                        mat->uniforms[i].data.f);
                break;
            case GL_FLOAT_VEC3:
                glUniform3fv(mat->uniforms[i].location, 1,
                        mat->uniforms[i].data.f);
                break;
            case GL_FLOAT_VEC4:
                glUniform4fv(mat->uniforms[i].location, 1,
                        mat->uniforms[i].data.f);
                break;
            case GL_SAMPLER_2D:
                texture_activate(mat->uniforms[i].data.texture, GL_TEXTURE_2D, texture_unit);

                glUniform1i(mat->uniforms[i].location, (int)texture_unit);
                texture_unit++;
                break;
            default:
                printf("DONT KNOW HOW TO UNIFORM 0x%02x\n", mat->uniforms[i].gl_type);
        }
    }
}

void
material_destroy(material_t *mat)
{
    assert(mat);
    asset_free(&mat->asset);
}
