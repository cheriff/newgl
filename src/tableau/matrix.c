#include <GLFW/glfw3.h>

#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>



#include "matrix.h"

printf_domain_t matrix_domain;

static int
do_arginfo (const struct printf_info *info, size_t n,
        int *argtypes)
{
    switch(info->spec) {
        case 'V':
        case 'M':
            if (n > 0)
                argtypes[0] = PA_POINTER;
            return 1;
        default:
            printf("unknown: %c\n", info->spec);
            return 0;
    }
}

static int
do_print (FILE *stream, const struct printf_info *info,
        const void *const *args)
{
    const vector_t *v;
    const matrix_t *m;
    switch(info->spec) {
        case 'V':
            v = *((const vector_t **) (args[0]));
            return fprintf(stream, "[%f %f %f %f]",
                    v->f[0],
                    v->f[1],
                    v->f[2],
                    v->f[3]);
        case 'M': {
            m = *((const matrix_t **) (args[0]));
            v = m->v;
            int ret = 0;
            ret += fprintf(stream, "[%f %f %f %f]",
                    v->f[0],
                    v->f[1],
                    v->f[2],
                    v->f[3]);
            v++;
            ret += fprintf(stream, "[%f %f %f %f]",
                    v->f[0],
                    v->f[1],
                    v->f[2],
                    v->f[3]);
            v++;
            ret += fprintf(stream, "[%f %f %f %f]",
                    v->f[0],
                    v->f[1],
                    v->f[2],
                    v->f[3]);

            v++;
            ret += fprintf(stream, "[%f %f %f %f]",
                    v->f[0],
                    v->f[1],
                    v->f[2],
                    v->f[3]);
            return ret;
            }
        default:
            return fprintf(stream, "WTF");
    };
}


void
matrix_init(void)
{
    matrix_domain = new_printf_domain();
    assert(matrix_domain);

    register_printf_domain_function (matrix_domain, 'V',
            do_print,
            do_arginfo, NULL);
}

void
matrixIdentity(matrix_t *m)
{
    memset(m, 0, sizeof(matrix_t));
    m->f[0] = m->f[5] = m->f[10] = m->f[15] = 1.0f;
}

void
vectorPrint(vector_t *v)
{
    printf("[%f %f %f %f]",
           v->f[0],
           v->f[1],
           v->f[2],
           v->f[3]);
}

void
matrixPrint(matrix_t *m)
{
    vectorPrint(m->v + 0); printf("\n");
    vectorPrint(m->v + 1); printf("\n");
    vectorPrint(m->v + 2); printf("\n");
    vectorPrint(m->v + 3); printf("\n");
}

/* C = A x B */
void
matrixMultiply(matrix_t *restrict c, matrix_t *restrict a, matrix_t *restrict b)
{
#define A(x,y) a->f[x+y*4]
#define B(x,y) b->f[x+y*4]
#define C(x,y) c->f[x+y*4]
    C(0,0) = A(0,0) * B(0,0) + A(0,1) * B(1,0) + A(0,2) * B(2,0) + A(0,3) * B(3,0); 
    C(0,1) = A(0,0) * B(0,1) + A(0,1) * B(1,1) + A(0,2) * B(2,1) + A(0,3) * B(3,1); 
    C(0,2) = A(0,0) * B(0,2) + A(0,1) * B(1,2) + A(0,2) * B(2,2) + A(0,3) * B(3,2); 
    C(0,3) = A(0,0) * B(0,3) + A(0,1) * B(1,3) + A(0,2) * B(2,3) + A(0,3) * B(3,3); 
    C(1,0) = A(1,0) * B(0,0) + A(1,1) * B(1,0) + A(1,2) * B(2,0) + A(1,3) * B(3,0); 
    C(1,1) = A(1,0) * B(0,1) + A(1,1) * B(1,1) + A(1,2) * B(2,1) + A(1,3) * B(3,1); 
    C(1,2) = A(1,0) * B(0,2) + A(1,1) * B(1,2) + A(1,2) * B(2,2) + A(1,3) * B(3,2); 
    C(1,3) = A(1,0) * B(0,3) + A(1,1) * B(1,3) + A(1,2) * B(2,3) + A(1,3) * B(3,3); 
    C(2,0) = A(2,0) * B(0,0) + A(2,1) * B(1,0) + A(2,2) * B(2,0) + A(2,3) * B(3,0); 
    C(2,1) = A(2,0) * B(0,1) + A(2,1) * B(1,1) + A(2,2) * B(2,1) + A(2,3) * B(3,1); 
    C(2,2) = A(2,0) * B(0,2) + A(2,1) * B(1,2) + A(2,2) * B(2,2) + A(2,3) * B(3,2); 
    C(2,3) = A(2,0) * B(0,3) + A(2,1) * B(1,3) + A(2,2) * B(2,3) + A(2,3) * B(3,3); 
    C(3,0) = A(3,0) * B(0,0) + A(3,1) * B(1,0) + A(3,2) * B(2,0) + A(3,3) * B(3,0); 
    C(3,1) = A(3,0) * B(0,1) + A(3,1) * B(1,1) + A(3,2) * B(2,1) + A(3,3) * B(3,1); 
    C(3,2) = A(3,0) * B(0,2) + A(3,1) * B(1,2) + A(3,2) * B(2,2) + A(3,3) * B(3,2); 
    C(3,3) = A(3,0) * B(0,3) + A(3,1) * B(1,3) + A(3,2) * B(2,3) + A(3,3) * B(3,3); 
#undef C
#undef B
#undef A
}

void
matrixScale(matrix_t *m, float x, float y, float z)
{
    matrix_t t, result;
    matrixIdentity(&t);
    t.f[0] = x;
    t.f[5] = y;
    t.f[10] = z;
    matrixMultiply(&result, m, &t);
    *m = result;
}

void
matrixTranslate(matrix_t *m, float x, float y, float z)
{
    matrix_t t, result;
    matrixIdentity(&t);
    t.f[12] = x;
    t.f[13] = y;
    t.f[14] = z;
    matrixMultiply(&result, m, &t);
    *m = result;
}



/*
 * x^2(1-c)+c     xy(1-c)-zs     xz(1-c)+ys     0
 * yx(1-c)+zs     y^2(1-c)+c     yz(1-c)-xs     0
 * xz(1-c)-ys     yz(1-c)+xs     z^2(1-c)+c     0
 * 0              0              0              1
 */
void
matrixRotate(matrix_t *m, float degrees, float x, float y, float z)
{
    const float radians = degrees / 180.0f * (float)M_PI; 
    const float c = cosf(radians);
    const float s = sinf(radians);
    
    float len = sqrtf(x*x + y*y + z*z);

    if ( len != 1.0f) {
        x /= len; 
        y /= len; 
        z /= len; 
    }

    matrix_t rot, result;
    rot.f[0]  = x*x*(1-c) + c;
    rot.f[4]  = x*y*(1-c) - z*s;
    rot.f[8]  = x*z*(1-c) + y*s;
    rot.f[12] = 0;

    rot.f[1] = y*x*(1-c) + z*s;
    rot.f[5] = y*y*(1-c) + c;
    rot.f[9] = y*z*(1-c) - x*s;
    rot.f[13] = 0;

    rot.f[2] = x*z*(1-c) - y*s;
    rot.f[6] = y*z*(1-c) + x*s;
    rot.f[10]= z*z*(1-c) + c;
    rot.f[14]= 0;

    rot.f[3] = 0;
    rot.f[7] = 0;
    rot.f[11] = 0;
    rot.f[15] = 1.0f;

    matrixMultiply(&result, m, &rot);
    *m = result;
}

/*
 * Sx  0  0  0
 * 0   Sy 0  0
 * 0   0  Sz -1
 * 0   0  Px 0
 *
 * Where:
 *    range = tan (fov * 0.5) * near
 *    Sx = (2 * near) / (range * aspect + range * aspect)
 *    Sy = near / range
 *    Sz = -(far + near) / (far - near)
 *    Pz = -(2 * far * near) / (far - near)
 */
void
matrixPerspective(matrix_t *m, float near, float far, float aspect, float fov)
{
    memset(m, 0, sizeof(matrix_t));
    const float range = tanf(fov * 0.5f) * near;
    const float Sx = (2.0f * near) / (range * aspect + range * aspect);
    const float Sy = near / range;
    const float Sz = -(far + near) / (far - near);
    const float Pz = -(2.0f * far * near) / (far - near);

    m->f[0] = Sx;
    m->f[5] = Sy;
    m->f[10] = Sz;
    m->f[14] = Pz;
    m->f[11] = -1.0f;
}

