#include <assert.h>
#include <stdlib.h>

#include <texture.h>
#include <dmalloc/dmalloc.h>

#include "texture_priv.h"

#include <GLFW/glfw3.h>

texture_t *
texture_lookup(assman_t *assets, char *name)
{
    asset_t *ret = assman_lookup(assets, name, ASS_TEXTURE);
    if (ret) return ret->asset_data;
    return NULL;
}

static void
_release(asset_t *a)
{
    assert(a);
    assert(a->type == ASS_TEXTURE);

    texture_t *t = a->asset_data;
    assert(t);

    assman_closeFile(t->file_asset);
}

static void
openGL_texture(texture_t *t)
{
    assert(t);
    assert(t->buffer == 0);

    glGenTextures (1, &t->buffer);
    glActiveTexture (GL_TEXTURE0);
    glBindTexture (GL_TEXTURE_2D, t->buffer);

    /* void glTexImage2D( GLenum target, GLint level, GLint internalformat,
     *                      GLsizei width, GLsizei height,
     *                      GLint border, GLenum format, GLenum  type,
     *                      const GLvoid *pixels);
     */
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA,
            t->width, t->height,
            0, GL_RGBA, GL_UNSIGNED_BYTE,
            t->image_data);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}

void 
texture_activate(texture_t *tex, unsigned type, unsigned pos)
{
    assert(tex);
    assert(pos < 48);
    assert(glGetError() == 0);

    int maxt;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxt);
    assert(glGetError() == 0);

    glActiveTexture(GL_TEXTURE0 + pos);
    assert(glGetError() == 0);

    glBindTexture(type, tex->buffer);
    assert(glGetError() == 0);
}

texture_t *
texture_load(assman_t *assets, char *name)
{
    assert(assets);
    assert(name);

    texture_t *ret = texture_lookup(assets, name);
    if (ret) return ret;

    file_asset_t *file_asset;
    char *path;

    asprintf(&path, "textures/%s.texb", name);

    file_asset = assman_openFile(assets, path);
    if (!file_asset) {
        free(path);
        printf("Texture not found: %s\n", name);
        return NULL;
    }

    ret = texb_load(file_asset->file_data, file_asset->size);
    assert(ret);

    ret->file_asset = file_asset;
    ret->asset.name = dstrdup(name);
    ret->asset.asset_data = ret;
    ret->asset.type = ASS_TEXTURE;
    ret->asset.release = _release;
    assman_add(assets, &ret->asset);

    openGL_texture(ret);

    free(path);

    return ret;
}

void
texture_destroy(texture_t *t)
{
    assert(t);
    asset_free(&t->asset);
}
