#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <objb.h>
#include <dmalloc/dmalloc.h>

#include "model_priv.h"

uint32_t objb_current_version = OBJB_CURRENT_VERSION;

void
_objb_release(model_t *model)
{
    assert(model);
    assert(model->groups);
    for (unsigned int i = 0; i < model->num_groups; i++) {
        group_t *g = model->groups + i;
        assert(g->material);
        material_destroy(g->material);
    }
    dfree(model->groups);
}

model_t *
_objb_load(char *data, size_t size)
{
    assert(data);
    if (size < sizeof(objb_header_t)) {
        printf("FILE TO SMALL\n");
        return NULL;
    }
    assert(((uintptr_t)data & 0xF) == 0);
    objb_header_t *header = (objb_header_t*)(void*)data;

    if (header->objb_magic != OBJB_MAGIC) {
        printf("BAD MAGIC: Got %08x; expected %08x\n",
                header->objb_magic, OBJB_MAGIC);
        return NULL;
    }

    if (header->objb_version != OBJB_CURRENT_VERSION) {
        printf("BAD VERSION: Got %d.%d; expected %d.%d\n",
                OBJB_VERSION_MAJ(header->objb_version),
                OBJB_VERSION_MIN(header->objb_version),
                OBJB_VERSION_MAJ(OBJB_CURRENT_VERSION),
                OBJB_VERSION_MIN(OBJB_CURRENT_VERSION));
        return NULL;
    }

    objb_group_t *groups = (void*)(data + header->groups_offset);

    model_t *ret = dmalloc(sizeof(*ret));

    ret->num_groups = header->num_groups;
    ret->groups = dmalloc(header->num_groups * sizeof(group_t));

    ret->num_indices = header->num_indices;
    ret->indices = (uint32_t*)(void*)(data + header->indices_offset);

    assert(sizeof(objb_vertex_t) == sizeof(vertex_t));
    ret->num_vertices = header->num_vertices;
    ret->vertices = (vertex_t*)(void*)(data + header->vertices_offset);

    unsigned int i;
    for(i=0; i<header->num_groups; i++) {
        group_t *g = ret->groups + i;
        g->name = groups[i].name;
        g->num_indices = groups[i].num_indices;
        g->index_base = groups[i].index_base;
    }

    return ret;
}

