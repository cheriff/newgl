#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <math.h>

#include <model.h>
#include <util.h>
#include <dmalloc/dmalloc.h>

#include "model_priv.h"

#include <GLFW/glfw3.h>


static int
model_bounds(model_t *model)
{
    float minx, miny, minz;
    minx = miny = minz = HUGE_VALF;

    float maxx, maxy, maxz;
    maxx = maxy = maxz = -HUGE_VALF;

    vertex_t *v = model->vertices;
    for(unsigned int i=0; i<model->num_vertices; i++, v++) {
        if (v->position.x > maxx) maxx = v->position.x;
        if (v->position.x < minx) minx = v->position.x;

        if (v->position.y > maxy) maxy = v->position.y;
        if (v->position.y < miny) miny = v->position.y;

        if (v->position.z > maxz) maxz = v->position.z;
        if (v->position.z < minz) minz = v->position.z;
    }

    model->center.f[0]= (maxx + minx) / 2.0f;
    model->center.f[1]= (maxy + miny) / 2.0f;
    model->center.f[2]= (maxz + minz) / 2.0f;

    model->bb.f[0] = (maxx - minx);
    model->bb.f[1] = (maxy - miny);
    model->bb.f[2] = (maxz - minz);

    return 0;
}

static int
model_opengl(model_t *model)
{
    assert(model);

    const GLsizeiptr vertex_size = model->num_vertices * sizeof(vertex_t);
    const GLsizeiptr indices_size = model->num_indices * sizeof(uint32_t);
    const GLsizeiptr buffer_size = vertex_size + indices_size;

    /* First off, create the VAO to capture bindings, pointer types/offsets for
     * ths model
     */
    glGenVertexArrays (1, &model->vao);
    glBindVertexArray (model->vao);

    // Generate one buffer for the model
    // We'll allocate enough size for vertex and index data, but not yet load with data
    glGenBuffers (1, &model->bo);
    glBindBuffer (GL_ARRAY_BUFFER, model->bo);
    glBufferData (GL_ARRAY_BUFFER, buffer_size, NULL, GL_STATIC_DRAW);

    // Now load in the data, noting that indices follow immediately after the
    // vertices
    model->index_offset = (uint64_t)vertex_size;
    glBufferSubData(GL_ARRAY_BUFFER, 0, vertex_size, model->vertices);
    glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)model->index_offset, indices_size, model->indices);

    // In order to use the indices portion of the buffer, we must bind it as an
    // element array too.
    // It seems OpenGL is happy with one buffer bound twice, we just have to
    // note the offset from the buffer to the indices. (there's nothing similar
    // to 'glVertexAttribPointer' for index data)
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, model->bo);

    // Finally, describe the vertex format. Use NULL instantiation of vertex_t
    // to calculate sizes and offsets.
    vertex_t *v = NULL;
    glEnableVertexAttribArray (0);
    glEnableVertexAttribArray (1);
    glEnableVertexAttribArray (2);
    glVertexAttribPointer (0, 4, GL_FLOAT, GL_FALSE, sizeof(*v), &v->position);
    glVertexAttribPointer (1, 4, GL_FLOAT, GL_FALSE, sizeof(*v), &v->normal);
    glVertexAttribPointer (2, 2, GL_FLOAT, GL_FALSE, sizeof(*v), &v->tex);

    for (unsigned int i = 0; i < model->num_groups; i++) {
        group_t *g = model->groups + i;

        // Convert base_vertex into a byte offset from the base of the BO, to
        // the groups first index.
        // So we skip past the vertex data and then convert the loaded base
        // index to bytes.
        g->index_base = model->index_offset + (g->index_base * sizeof(uint32_t));

        if (g->material == NULL) {
            g->material = material_load(model->asset.manager, "default");
            if (!g->material) {
                printf("MATERIAL FAILED\n");
                return 1;
            }
        }

    }
    return 0;
}

model_t *
model_lookup(assman_t *assets, char *name)
{
    asset_t *ret = assman_lookup(assets, name, ASS_MODEL);
    if (ret) return ret->asset_data;
    return NULL;
}

static void
_release(asset_t *a)
{
    assert(a);
    assert(a->type == ASS_MODEL);

    model_t *m = a->asset_data;
    assert(m);

    /* Models created by hand won't have an associated file */
    if(m->file_asset) {
        assman_closeFile(m->file_asset);
    }

    _objb_release(m);
}
model_t *
model_load(assman_t *assets, char *name)
{
    assert(assets);
    assert(name);

    model_t *ret = model_lookup(assets, name);
    if (ret) return ret;

    file_asset_t *file_asset;
    char *path;

    // First we look for the optimised objb file
    asprintf(&path, "models/%s.objb", name);

    file_asset = assman_openFile(assets, path);
    if (!file_asset) {
        printf("Model not found: %s\n", name);
        free(path);
        return NULL;
    }
    
    ret = _objb_load(file_asset->file_data,
            file_asset->size);

    if (ret) {
        ret->file_asset = file_asset;
        ret->asset.name = dstrdup(name);
        ret->asset.asset_data = ret;
        ret->asset.type = ASS_MODEL;
        ret->asset.release = _release;
        assman_add(assets, &ret->asset);
        model_bounds(ret);
        if (model_opengl(ret)) {
            // Leaky, but this means bad default material so we're screwed anyway
            return NULL;
        }
    }
    free(path);
    return ret;
}


void
model_draw(int pass, scene_t *scene, void *data)
{
    assert(scene);
    (void)pass;

    model_t *model = (model_t*)data;

    glBindVertexArray (model->vao);

    for (unsigned int i = 0; i < model->num_groups; i++) {
        group_t *g = model->groups + i;
        material_activate(scene, g->material);
        glDrawElements(GL_TRIANGLES,
                (GLsizei)g->num_indices,
                GL_UNSIGNED_INT,
                (void*)(g->index_base));
    }
}

void
model_draw_wireframe(int pass, scene_t *scene, void *data)
{
    assert(scene);
    (void)pass;

    model_t *model = (model_t*)data;
    glBindVertexArray (model->vao);
    material_activate(scene, model->groups->material);

    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

    glDrawElements(GL_TRIANGLES,
            (GLsizei)model->num_indices,
            GL_UNSIGNED_INT,
            (void*)model->index_offset);
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void
model_draw_points(int pass, scene_t *scene, void *data)
{
    assert(scene);
    (void)pass;

    model_t *model = (model_t*)data;
    glBindVertexArray (model->vao);
    material_activate(scene, model->groups->material);

    glDrawElements(GL_POINTS,
            (GLsizei)model->num_indices,
            GL_UNSIGNED_INT,
            (void*)model->index_offset);
}

group_t *
model_groupByName(model_t *model, char *name)
{
    assert(model);
    assert(name);
    for (unsigned int i = 0; i < model->num_groups; i++) {
        group_t *g = model->groups + i;
        if (strcmp(g->name, name) == 0) return g;
    }
    return NULL;
}

void
model_destroy(model_t *m)
{
    assert(m);
    asset_free(&m->asset);
}

model_t *
model_create(assman_t *assets, char *name,
        vertex_t *vertices, uint32_t num_vertices, uint32_t *indices, uint32_t num_indices,
        material_t *material)
{
    model_t *ret = dmalloc(sizeof(*ret));

    ret->asset.name = dstrdup(name);
    ret->asset.asset_data = ret;
    ret->asset.type = ASS_MODEL;
    ret->asset.release = _release;
    assman_add(assets, &ret->asset);

    ret->groups = dmalloc(sizeof(group_t));
    ret->num_groups = 1;
    ret->groups->num_indices = num_indices;
    ret->groups->index_base = 0;
    ret->groups->name = name;
    ret->groups->material = material;

    ret->num_vertices = num_vertices;
    ret->vertices = vertices;

    ret->num_indices = num_indices;
    ret->indices = indices;

    model_bounds(ret);
    if (model_opengl(ret)) {
        // Leaky, but this means bad material so we're screwed anyway
        return NULL;
    }

    return ret;
}
