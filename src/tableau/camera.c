#include <stdio.h>
#include <assert.h>

#include <camera.h>

#include <dmalloc/dmalloc.h>

#include "scene_priv.h"


void
camera_forward(scene_t *scene, float f)
{
    assert(scene);
    matrix_t *mat = &scene->view;
    vector_t step;
    vector_mul(&step, mat->v[2], f);
    vector_add(&mat->v[3], mat->v[3], step);
}

void
camera_strafe(scene_t *scene, float f)
{
    assert(scene);
    matrix_t *mat = &scene->view;
    vector_t step;
    vector_mul(&step, mat->v[0], f);
    vector_add(&mat->v[3], mat->v[3], step);
}
