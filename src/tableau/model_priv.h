#ifndef __MODEL_PRIV_H__
#define __MODEL_PRIV_H__

#include <model.h>

model_t *_objb_load(char *data, size_t size);
void _objb_release(model_t *model);
void model_dump(model_t *m);

#endif
