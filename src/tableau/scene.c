#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <dmalloc/dmalloc.h>
#include <matrix.h>
#include <scene.h>

#include "scene_priv.h"



scene_t *
sceneCreate(void)
{
    scene_t *ret = dmalloc(sizeof(*ret));
    ret->root.scene = ret;
    matrixIdentity(&ret->root.matrix);
    ret->matrix_stack_size = 1; // Always keep proj, view and root node matrices handy
    ret->matrix_stack = dmalloc(sizeof(matrix_t) * ret->matrix_stack_size);
    matrixIdentity(&ret->projection);
    matrixIdentity(&ret->view);
    return ret;
}

matrix_t *
scene_getMatrix(scene_t *scene, unsigned int which)
{
    assert(scene);
    switch (which) {
        case CAMERA_PROJECTION_MATRIX:
            return &scene->projection;
        case CAMERA_VIEW_MATRIX:
            return &scene->view;
        default:
            assert(which < scene->matrix_stack_size);
            return scene->matrix_stack + which;
    }
}

static void
freeChildren(sceneNode_t *node)
{
    sceneNode_t *child, *next;
    for (child = node->children; child != NULL; child=next) {
        freeChildren(child);
        next = child->next;
        dfree(child->name);
        dfree(child);
    }
}

void
sceneDestroy(scene_t *scene)
{
    assert(scene);

    freeChildren(&scene->root);
    dfree(scene->matrix_stack);
    dfree(scene);
}

sceneNode_t *
scene_getRootNode(scene_t *scene)
{
    assert(scene);
    return &(scene->root);
}

sceneNode_t *
sceneNodeCreate(char *name)
{
    sceneNode_t *ret = dmalloc(sizeof(*ret));
    if (name) {
        ret->name = dstrdup(name);
    }
    matrixIdentity(&ret->matrix);
    return ret;
}

void
sceneNode_add(sceneNode_t *parent, sceneNode_t *child)
{
    assert(parent);
    assert(child);
    assert(child->parent == NULL);
    assert(child->scene == NULL);

    scene_t *scene = parent->scene;
    assert(scene != NULL);

    child->parent = parent;
    child->depth = parent->depth + 1;
    child->scene = scene;
    child->next = parent->children;
    parent->children = child;

    if (child->depth == scene->matrix_stack_size) {
        scene->matrix_stack_size++;
        size_t alloc_size = scene->matrix_stack_size * sizeof(matrix_t);
        scene->matrix_stack = drealloc(scene->matrix_stack, alloc_size);
    }
}

void
sceneNode_identity(sceneNode_t *node)
{
    assert(node);
    matrixIdentity(&node->matrix);
}

void
sceneNode_translate(sceneNode_t *node,
                float x, float y, float z)
{
    assert(node);
    matrixTranslate(&node->matrix, x, y, z);
}

void
sceneNode_scale(sceneNode_t *node,
                float x, float y, float z)
{
    assert(node);
    matrixScale(&node->matrix, x, y, z);
}

void
sceneNode_rotate(sceneNode_t *node,
                float angle,
                float x, float y, float z)
{
    assert(node);
    matrixRotate(&node->matrix, angle, x, y, z);
}

void
sceneNode_setRender(sceneNode_t *node, drawFunction_t function, void *data)
{
    assert(node);
    node->func = function;
    node->user_data = data;
}

void
scene_dump(scene_t* scene)
{
    assert(scene);
    printf("ROOT:\n");
}

static void
sceneNode_draw(sceneNode_t *node, int pass, unsigned int depth)
{
    assert(node);

    scene_t *scene = node->scene;
    assert(scene);
    assert(depth > 0);
    assert(depth < scene->matrix_stack_size);
    matrixMultiply(scene->matrix_stack + depth,
                   scene->matrix_stack + (depth-1),
                   &node->matrix);

    if (node->func) {
        scene->current_depth = depth;
        assert(scene->current_depth == node->depth);
        node->func(pass, scene, node->user_data);
    }

    sceneNode_t *child;
    for (child = node->children; child != NULL; child=child->next) {
        sceneNode_draw(child, pass, depth+1);
    }

}

void
scene_draw(scene_t *scene, int pass)
{
    assert(scene);

    const int depth = 0;
    const int index = CAMERA_MATRIX_STACK(depth);
    sceneNode_t *node = &scene->root;
    matrixMultiply(scene->matrix_stack + index,
                   &node->matrix,
                   &scene->view
                   );
    if (node->func) {
        scene->current_depth = depth;
        node->func(pass, scene, node->user_data);
    }

    sceneNode_t *child;
    for (child = node->children; child != NULL; child=child->next) {
        sceneNode_draw(child, pass, depth + 1);
    }
}
