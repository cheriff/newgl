#include <assert.h>

#include <texture.h>
#include <texb.h>
#include "texture_priv.h"

#include <dmalloc/dmalloc.h>

uint32_t texb_current_version = TEXB_CURRENT_VERSION;


texture_t *
texb_load(char *data, size_t size)
{
    assert(data);

    if (size < sizeof(texb_header_t)) {
        printf("File smaller than texb header\n");
        return NULL;
    }

    texb_header_t *header = (texb_header_t*)(void*)data;
    if (header->texb_magic != TEXB_MAGIC) {
        printf("Bad TexB magic: Got %08x, expected %08x\n",
                header->texb_magic, TEXB_MAGIC);
        return NULL;
    }

    if (header->texb_version != texb_current_version) {
        printf("Bad version: Got %d.%d, expected %d.%d\n",
                TEXB_VERSION_MAJ(header->texb_version),
                TEXB_VERSION_MIN(header->texb_version),
                TEXB_VERSION_MIN(texb_current_version),
                TEXB_VERSION_MIN(texb_current_version));
        return NULL;
    }

    uint32_t img_size =  header->width * header->height * header->depth;
    if (size < (sizeof(texb_header_t) + img_size)) {
        printf("File to small to contain described image\n");
        return NULL;
    }

    texture_t *ret = dmalloc(sizeof(*ret));
    ret->width = header->width;
    ret->height = header->height;
    ret->depth = header->depth;

    ret->image_size = img_size;
    ret->image_data = data + sizeof(texb_header_t);

    return ret;
}
