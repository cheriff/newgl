#ifndef __TEXTURE_PRIV_H__
#define __TEXTURE_PRIV_H__

#include <texture.h>

texture_t *texb_load(char *data, size_t size);

struct texture {
    asset_t asset;
    file_asset_t *file_asset;

    void *image_data;

    uint16_t width;
    uint16_t height;
    uint16_t depth;
    uint16_t pad;

    uint32_t buffer;
    uint32_t image_size;
};

#endif
