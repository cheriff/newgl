#include <sys/mman.h>
#include <unistd.h>

#include <fcntl.h>
#include <assert.h>
#include <stdio.h>

#include <util.h>

size_t
util_fileSize(const char *path)
{
    assert(path);

    FILE *fp = fopen(path, "r");
    if (!fp) {
        printf("NO SUCH FILE: %s\n", path);
        return 0;
    }

    int result = fseek(fp, 0, SEEK_END);
    if (result) {
        perror("fseek");
        assert(result == 0);
    }

    long size = ftell(fp);
    if (size < 0) {
        perror("fseek");
        assert(size >= 0);
    }

    fclose(fp);

    return (size_t)size;
}

void
util_munmap(void *p, size_t len, int fd)
{
    assert(p);
    munmap(p, len);
    if (fd) close(fd);
}


void*
util_mmap(const char *path, size_t *size_out, int *fdout)
{
    assert(path);
    size_t size = util_fileSize(path);
    *size_out = size;
    if (size == 0) {
        return NULL;
    }

    int fd = open(path, O_RDONLY);
    void *data = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    if (data == MAP_FAILED) {
        perror("mmap");
        *size_out = 0;
        return NULL;
    }

    if (fdout) {
        *fdout = fd;
    } else {
        close (fd);
    }

    return data;
}

char *
util_strSize(size_t size)
{
    static char buff[1024];
    if (size < 1024) {
        snprintf(buff, 1024, "%zu Bytes", size);
        return buff;
    }

#define FLOAT_SPECIFER "%2.2f"

    float kb = size / 1024.0f;
    if (kb < 1024) {
        snprintf(buff, 1024, FLOAT_SPECIFER " KB", kb);
        return buff;
    }

    float mb = kb / 1024.0f;
    if (mb < 1024) {
        snprintf(buff, 1024, FLOAT_SPECIFER " MB", mb);
        return buff;
    }

    float gb = mb / 1024.0f;
    if (gb < 1024) {
        snprintf(buff, 1024, FLOAT_SPECIFER " GB", gb);
        return buff;
    }

    float tb = gb / 1024.0f;
    snprintf(buff, 1024, FLOAT_SPECIFER " TB", tb);
    return buff;
}
