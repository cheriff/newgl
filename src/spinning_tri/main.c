#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <GLFW/glfw3.h>

#include <framerate.h>
#include <assets.h>
#include <matrix.h>
#include <scene.h>
#include <camera.h>
#include <texture.h>
#include <util.h>
#include <model.h>

#include <dmalloc/dmalloc.h>


static int unused UNUSED;
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

static struct global {
    matrix_t *projection;
} GLOBAL = { NULL };


static void NORETURN
errorHappened(int code, const char* str)
{
    printf("GLFW Error(%d) :: %s\n", code, str);
    exit(-1);
}

static void
windowMoved(GLFWwindow *win, int x, int y)
{
    printf("MOVE: Window %p:  %d %d\n", win, x, y);
}

static void
windowResized(GLFWwindow *win, int x, int y)
{
    printf("SIZE: Window %p:  %d %d\n", win, x, y);
    float sizeX = (float)x;
    float sizeY = (float)y;
    matrixPerspective(GLOBAL.projection,
            0.1f, 1000.0f,
            sizeX/sizeY,
            45.0);
}



static void
myDrawBackground(int pass UNUSED, scene_t *s UNUSED, void *data UNUSED)
{
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

int
main(int argc, char *argv[])
{
    int ret = GL_TRUE;
    matrix_init();
    assman_t *assets = assman_init(argc, argv);

    atexit(dmalloc_analyze);

    ret = glfwInit();
    if (ret != GL_TRUE) {
        printf("Fail\n");
    }

    //Request Specific Version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    // Open OpenGL window
    GLFWwindow *window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT,
            argv[0], NULL, NULL);
     if (!window)
     {
         fprintf(stderr, "Failed to open GLFW window\n");
         ret = GL_FALSE;
         goto exit;

     }
     glfwSetWindowPosCallback(window, windowMoved);
     glfwSetWindowSizeCallback(window, windowResized);
     glfwSetErrorCallback(errorHappened);
     glfwMakeContextCurrent(window);



     struct vertex vertices[] = {
         {           //Top middle
         .position = { 0.0f,  0.5f,  0.0f, 1.0f, },
         .normal   = { 0.0f,  0.0f,  1.0f, 0.0f, },
         .tex      = { 0.5, -0.4f},
         },
         {           //bottom right
         .position = { 0.5f, -0.5f,  0.0f, 1.0f, },
         .normal   = { 0.0f,  0.0f,  1.0f, 0.0f, },
         .tex      = { 1, 1 },
         },
         {           //bottom left
         .position = { -0.5f, -0.5f,  0.0f, 1.0f, },
         .normal   = { 0.0f,  0.0f,  1.0f, 0.0f, },
         .tex      = { 0, 1 },
         },
     };

     uint32_t indices[] = { 0, 1, 2 };

     material_t * material = material_load(assets, "jay");
     assert(material);

     model_t *model = model_create(assets, "triangle", vertices, 3, indices, 3, material);
     if (!model) {
         printf("Model create failed\n");
         return -1;
     }


     glfwSwapInterval(1);

     glEnable (GL_DEPTH_TEST);
     glDepthFunc (GL_LESS);

     FrameTime frameTime;
     InitFrameTime(&frameTime);

     scene_t *scene = sceneCreate();

     sceneNode_t *rootNode = scene_getRootNode(scene);
     sceneNode_setRender(rootNode, myDrawBackground, NULL);
     GLOBAL.projection = scene_getMatrix(scene, CAMERA_PROJECTION_MATRIX);
     windowResized(window, WINDOW_WIDTH, WINDOW_HEIGHT);


     sceneNode_t *model_node = sceneNodeCreate("model_node");
     sceneNode_setRender(model_node,model_draw, model);
     sceneNode_add(rootNode, model_node);

     camera_back(scene, 1);


     float lasttime = 0;
     float rot = 0;
     while (!glfwWindowShouldClose (window)) {
         assman_update(assets);
         float time = frameStart(&frameTime);
         float delta = lasttime - time;
         lasttime = time;


         glfwPollEvents();
         float scale = 10;
         if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
             scale *= 10;
         }
         if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS) {
             scale *= 1000;
         }
         rot += delta*scale;

         sceneNode_identity(model_node);
         sceneNode_rotate(model_node, 180.0, 0.0f, 1.0f, 0.0f);
         sceneNode_rotate(model_node, rot, 0.0f, 1.0f, 0.0f);
         sceneNode_scale(model_node, 0.5f, 0.5f, 0.5f);


         static int mode = 0;
         static int lastq = 0;
         if (glfwGetKey(window, 'Q') == GLFW_PRESS) {
             static struct {
                 drawFunction_t func;
                 char *name;
             } modes[] = {
                 { model_draw, "Draw" },
                 { model_draw_wireframe, "WireFrame" },
                 { model_draw_points, "Points" },
             };
             if (!lastq) {
                 mode++;
                 if (mode == 3) mode = 0;
                 sceneNode_setRender(model_node, modes[mode].func, model);
                 printf("Mode: %d %s\n", mode, modes[mode].name);
                 lastq = 1;
             }
         }
         else lastq = 0;

         scene_draw(scene, 0);
         char *c = frameEnd(&frameTime);

         if (c) glfwSetWindowTitle(window, c);

         glfwSwapBuffers(window);
         if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) glfwSetWindowShouldClose(window, 1);
     } 

     model_destroy(model);
     sceneDestroy(scene);

     assman_debug_list(assets);
     assman_destroy(assets);

exit:
    glfwTerminate();
    return ret != GL_TRUE;
}

