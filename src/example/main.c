#include <stdio.h>
#include <stdlib.h>

#include <GLFW/glfw3.h>

#include <framerate.h>
#include <assets.h>
#include <matrix.h>
#include <scene.h>
#include <camera.h>
#include <util.h>
#include <model.h>

#include <dmalloc/dmalloc.h>


static int unused UNUSED;
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

static struct global {
    matrix_t *projection;
} GLOBAL = { NULL };

struct drawState {
    char *debug_name;
    unsigned int vao;
    int proj_location;
    int view_location;
    int _pad0;
};

static void NORETURN
errorHappened(int code, const char* str)
{
    printf("GLFW Error(%d) :: %s\n", code, str);
    exit(-1);
}

static void
windowMoved(GLFWwindow *win, int x, int y)
{
    printf("MOVE: Window %p:  %d %d\n", win, x, y);
}

static void
windowResized(GLFWwindow *win, int x, int y)
{
    printf("SIZE: Window %p:  %d %d\n", win, x, y);
    float sizeX = (float)x;
    float sizeY = (float)y;
    matrixPerspective(GLOBAL.projection,
            0.1f, 1000.0f,
            sizeX/sizeY,
            45.0);
}

static void
dump_modes(GLFWmonitor *m)
{
    int i, count, found_current = 0;
    const GLFWvidmode *current = glfwGetVideoMode(m);
    const GLFWvidmode *modes = glfwGetVideoModes(m, &count);
    printf("\tModes: %d (* indicates current mode)\n", count);
    for(i=0; i<count; i++) {
        int isCurrent = !memcmp(modes+i, current, sizeof(GLFWvidmode));
        found_current += isCurrent;
        printf("\t   %c %dx%d @ %dHz. (%d:%d:%d)\t%p\n",
                (isCurrent)?'*':' ',
                modes[i].width, modes[i].height,
                modes[i].refreshRate,
                modes[i].redBits, modes[i].greenBits, modes[i].blueBits, modes+i);
    }
    if (!found_current) 
        printf("\nCURRENT\t   %dx%d @ %dHz. (%d:%d:%d)\t%p\n",
                current->width, current->height,
                current->refreshRate,
                current->redBits, current->greenBits, current->blueBits, current);
}

static void
dump_monitors(void)
{

    int num_monitors;
    GLFWmonitor** monitors =  glfwGetMonitors(&num_monitors);
    for (int i=0; i<num_monitors; i++) {
        int x, y;
        glfwGetMonitorPhysicalSize (monitors[i], &x, &y);
        printf("\tPhysical Size: %dmm  x %dmm\n", x, y);

        glfwGetMonitorPos (monitors[i], &x, &y);
        printf("\tPosition: %d %d\n", x, y);

        dump_modes(monitors[i]);
    }
}

#if 0
static void
myDraw(int pass UNUSED, matrix_t *m, void *data)
{
    struct drawState *st = (struct drawState*)data;
    glUniformMatrix4fv (st->view_location, 1, GL_FALSE, m->f);
    glBindVertexArray (st->vao);
    glDrawArrays (GL_TRIANGLES, 0, 3);
}
#endif


static void
myDrawBackground(int pass UNUSED, scene_t *s UNUSED, void *data UNUSED)
{
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

int
main(int argc, char *argv[])
{
    int ret = GL_TRUE;
    char *model_name;
    assert(argc > 1);
    model_name = argv[1];

    matrix_init();

    assman_t *assets = assman_init(argc, argv);

    ret = glfwInit();
    if (ret != GL_TRUE) {
        printf("Fail\n");
    }

    //Request Specific Version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

     if (argc==0) dump_monitors();

    // Open OpenGL window
    GLFWwindow *window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT,
            argv[0], NULL, NULL);
     if (!window)
     {
         fprintf(stderr, "Failed to open GLFW window\n");
         ret = GL_FALSE;
         goto exit;

     }
     glfwSetWindowPosCallback(window, windowMoved);
     glfwSetWindowSizeCallback(window, windowResized);
     glfwSetErrorCallback(errorHappened);
     glfwMakeContextCurrent(window);

     const GLubyte* renderer = glGetString (GL_RENDERER); // get renderer string
     const GLubyte* version = glGetString (GL_VERSION); // version as a string
     printf ("Renderer: %s\n", renderer);
     printf ("OpenGL version supported %s\n", version);

     int major, minor, revision;
     major = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MAJOR);
     minor = glfwGetWindowAttrib(window, GLFW_CONTEXT_VERSION_MINOR);
     revision = glfwGetWindowAttrib(window, GLFW_CONTEXT_REVISION);
     printf("Have OpenGL version: %d.%d r%d\n",
             major, minor, revision);

     model_t *model = model_load(assets, model_name);
     if (!model) {
         printf("Model open failed\n");
         return -1;
     }


     glfwSwapInterval(1);

     glEnable (GL_DEPTH_TEST);
     glDepthFunc (GL_LESS);

#if 0
     float points[] = {
         0.0f,  0.5f,  0.0f,
         0.5f, -0.5f,  0.0f,
         -0.5f, -0.5f,  0.0f
     };
     float colours[] = {
         1.0f, 0.0f,  0.0f,
         0.0f, 1.0f,  0.0f,
         0.0f, 0.0f,  1.0f
     };

     unsigned int vbo = 0;
     glGenBuffers (1, &vbo);
     glBindBuffer (GL_ARRAY_BUFFER, vbo);
     glBufferData (GL_ARRAY_BUFFER, 9 * sizeof (float), points, GL_STATIC_DRAW);

     unsigned int col_vbo = 0;
     glGenBuffers (1, &col_vbo);
     glBindBuffer (GL_ARRAY_BUFFER, col_vbo);
     glBufferData (GL_ARRAY_BUFFER, 9 * sizeof (float), colours, GL_STATIC_DRAW);

     /*  In older GL implementations we would have to bind each one, and define
      *  their memory layout, every time that we draw the mesh. To simplify
      *  that, we have new thing called the vertex attribute object (VAO), which
      *  remembers all of the vertex buffers that you want to use, and the
      *  memory layout of each one. We set up the vertex array object once per
      *  mesh. When we want to draw, all we do then is bind the VAO and draw.
      */
     unsigned int vao = 0;
     glGenVertexArrays (1, &vao);
     glBindVertexArray (vao);
     glBindBuffer (GL_ARRAY_BUFFER, vbo);
     glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
     glBindBuffer (GL_ARRAY_BUFFER, col_vbo);
     glVertexAttribPointer (1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

     glEnableVertexAttribArray (0);
     glEnableVertexAttribArray (1);
#endif


     FrameTime time;
     InitFrameTime(&time);
     float rot  = 0.0f;

     scene_t *scene = sceneCreate();

     sceneNode_t *rootNode = scene_getRootNode(scene);
     sceneNode_setRender(rootNode, myDrawBackground, NULL);
     GLOBAL.projection = scene_getMatrix(scene, CAMERA_PROJECTION_MATRIX);
     windowResized(window, WINDOW_WIDTH, WINDOW_HEIGHT);


     sceneNode_t *group = sceneNodeCreate("group");
     sceneNode_setRender(group,model_draw, model);

     sceneNode_add(rootNode, group);


     while (!glfwWindowShouldClose (window)) {
         assman_update(assets);

         sceneNode_identity(group);
         sceneNode_rotate(group, rot, 0.0f, 1.0f, 0.0f);
         sceneNode_scale(group, 0.5f, 0.5f, 0.5f);

         rot += 0.1f;

         glfwPollEvents();
         float amt = 1.0f;
         if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
             amt *= 2;
         }
         if (glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS) {
             amt *= 2;
         }
         if (glfwGetKey(window, 'W') == GLFW_PRESS) {
             camera_forward(scene, amt);
         }
         if (glfwGetKey(window, 'S') == GLFW_PRESS) {
             camera_back(scene, amt);
         }
         if (glfwGetKey(window, 'A') == GLFW_PRESS) {
             camera_strafe(scene, amt);
         }
         if (glfwGetKey(window, 'D') == GLFW_PRESS) {
             camera_strafe(scene, -amt);
         }


         static int mode = 0;
         static int lastq = 0;
         if (glfwGetKey(window, 'Q') == GLFW_PRESS) {
             static struct {
                 drawFunction_t func;
                 char *name;
             } modes[] = {
                 { model_draw, "Draw" },
                 { model_draw_wireframe, "WireFrame" },
                 { model_draw_points, "Points" },
             };
             if (!lastq) {
                 mode++;
                 if (mode == 3) mode = 0;
                 sceneNode_setRender(group, modes[mode].func, model);
                 printf("Mode: %d %s\n", mode, modes[mode].name);
                 lastq = 1;
             }
         }
         else lastq = 0;

         frameStart(&time);
         scene_draw(scene, 0);
         char *c = frameEnd(&time);

         if (c) glfwSetWindowTitle(window, c);

         glfwSwapBuffers(window);
         if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) glfwSetWindowShouldClose(window, 1);
     } 

     model_destroy(model);

     sceneDestroy(scene);

     assman_debug_list(assets);
     assman_destroy(assets);

exit:
    dmalloc_analyze();
    glfwTerminate();
    return ret != GL_TRUE;
}

