#ifndef __TEXB_H__
#define __TEXB_H__

#include <stdlib.h>
#include <texb.h>

#define TEXB_NAME_SZ 128
// Magic number: 'TEXb'
#define TEXB_MAGIC  0x54455862

#define TEXB_VERSION(maj, min)  (((min)&0xFFFF) | (((maj)&0xFFFF)<<16))
#define TEXB_VERSION_MAJ(ver) (((ver)>>16) & 0xFFFF)
#define TEXB_VERSION_MIN(ver) (((ver)>>0) & 0xFFFF)

#define TEXB_CURRENT_VERSION  TEXB_VERSION(0,1)
/* TEXB version history:
 *  0.1: Initial implementation
 */
extern uint32_t texb_current_version;

typedef struct texb_header {
    uint32_t texb_magic;
    uint32_t texb_version;

    uint16_t width;
    uint16_t height;
    uint16_t depth;
    uint16_t pad;
} texb_header_t;




#endif
