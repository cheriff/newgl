#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <matrix.h>
#include <scene.h>

typedef matrix_t camera_t;


void camera_forward(scene_t *scene, float amt);
#define camera_back(scene, amt) camera_forward( (scene), -(amt))

void camera_strafe(scene_t *scene, float amt);

#endif
