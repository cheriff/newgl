#ifndef __SCENE_H__
#define __SCENE_H__

#include <matrix.h>

#define CAMERA_PROJECTION_MATRIX 0x800
#define CAMERA_VIEW_MATRIX 0x801
#define CAMERA_MATRIX_STACK(n) ((n))


typedef struct scene scene_t;
typedef struct sceneNode sceneNode_t;

scene_t *sceneCreate(void);

matrix_t * scene_getMatrix(scene_t *scane, unsigned int which);

void sceneDestroy(scene_t *scene);

sceneNode_t *sceneNodeCreate(char *name);
sceneNode_t *scene_getRootNode(scene_t *scene);

typedef void (*drawFunction_t)(int pass, scene_t *scene, void *data);
void sceneNode_add(sceneNode_t *parent, sceneNode_t *child);
void sceneNode_setRender(sceneNode_t *node, drawFunction_t function, void *data);

void scene_dump(scene_t* scene);

void sceneNode_rotate(sceneNode_t *node,
                float angle,
                float x, float y, float z);
void sceneNode_translate(sceneNode_t *node,
                float x, float y, float z);

void sceneNode_scale(sceneNode_t *node,
                float x, float y, float z);

void sceneNode_setMatrix(sceneNode_t *node, matrix_t *m);
void sceneNode_identity(sceneNode_t *node);

void scene_draw(scene_t *scene, int pass);

#endif
