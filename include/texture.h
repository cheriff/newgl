#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <stdint.h>

#include <assets.h>

typedef struct texture texture_t;


texture_t *texture_load(assman_t *assets, char *name);

texture_t *texture_lookup(assman_t *assets, char *name);

void texture_destroy(texture_t *t);
void texture_activate(texture_t *t, unsigned type, unsigned tex_unit);

#endif
