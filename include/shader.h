#ifndef __SHADER_H__
#define __SHADER_H__

#include <assets.h> 
#include <scene.h> 

#include <GLFW/glfw3.h>
#include <assert.h>

typedef struct shader shader_t;
typedef struct uniform_info uniform_info_t;

struct shader {
    asset_t asset;

    file_asset_t *vs_file_asset;
    file_asset_t *fs_file_asset;
    unsigned int program;
    unsigned int vs;
    unsigned int fs;

    int projection_location;
    int view_location;

    unsigned int old_program;
    int is_active;
    int _pad0;
};

struct uniform_info {
    GLint location;
    unsigned int type;
    GLint size;
    unsigned int pad;
};


shader_t *shader_load(assman_t *assets, char *name);
shader_t *shader_lookup(assman_t *assets, char *name);
void shader_destroy(shader_t * sh);

int shader_activate(scene_t *scene, shader_t *shader);
int shader_uniform_info(shader_t *shader, uniform_info_t *info, char *name);

static inline char *
glType_to_string(GLenum type)
{
    switch(type) {
#define ITEM(name, num) case num: return #name;
    ITEM(GL_FLOAT_VEC2       , 0x8B50)
    ITEM(GL_FLOAT_VEC3       , 0x8B51)
    ITEM(GL_FLOAT_VEC4       , 0x8B52)
    ITEM(GL_INT_VEC2         , 0x8B53)
    ITEM(GL_INT_VEC3         , 0x8B54)
    ITEM(GL_INT_VEC4         , 0x8B55)
    ITEM(GL_BOOL             , 0x8B56)
    ITEM(GL_BOOL_VEC2        , 0x8B57)
    ITEM(GL_BOOL_VEC3        , 0x8B58)
    ITEM(GL_BOOL_VEC4        , 0x8B59)
    ITEM(GL_FLOAT_MAT2       , 0x8B5A)
    ITEM(GL_FLOAT_MAT3       , 0x8B5B)
    ITEM(GL_FLOAT_MAT4       , 0x8B5C)
    ITEM(GL_SAMPLER_1D       , 0x8B5D)
    ITEM(GL_SAMPLER_2D       , 0x8B5E)
    ITEM(GL_SAMPLER_3D       , 0x8B5F)
    ITEM(GL_SAMPLER_CUBE     , 0x8B60)
    ITEM(GL_SAMPLER_1D_SHADOW, 0x8B61)
    ITEM(GL_SAMPLER_2D_SHADOW, 0x8B62)
    ITEM(GL_BYTE             , 0x1400)
    ITEM(GL_UNSIGNED_BYTE    , 0x1401)
    ITEM(GL_SHORT            , 0x1402)
    ITEM(GL_UNSIGNED_SHORT   , 0x1403)
    ITEM(GL_INT              , 0x1404)
    ITEM(GL_UNSIGNED_INT     , 0x1405)
    ITEM(GL_FLOAT            , 0x1406)
    ITEM(GL_DOUBLE           , 0x140A)

#undef ITEM
        default:
            printf("Unknown: %04x\n", type);
            assert(!"Fail");
    }
}

#endif
