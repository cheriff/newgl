#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdlib.h>

void* util_mmap(const char *path, size_t *size, int *fd);
void util_munmap(void *p, size_t len, int fd);
size_t util_fileSize(const char *path);
char * util_strSize(size_t size);

#define UNUSED   __attribute__((unused))
#define NORETURN __attribute__((noreturn))


#endif
