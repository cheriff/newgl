#ifndef __MODEL_H__
#define __MODEL_H__

#include <stdint.h>
#include <stdio.h>

#include <matrix.h>
#include <assets.h>
#include <material.h>

typedef struct model model_t;
typedef struct group group_t;
typedef struct vertex vertex_t;

struct vertex {
    struct {
        float x, y, z, w;
    } position;
    struct {
        float x, y, z, w;
    } normal;
    struct {
        float s, t;
    } tex;
};

struct model {
    asset_t asset;

    uint32_t num_groups;

    uint32_t vao; // holds the format and offsets, etc within the bo
    uint32_t bo; // vertex data, then indices
    uint32_t pad0;
    uint64_t index_offset; // byte offset to in indices from the base of the BO.
                           // Essentially the length of the vertices data
                           // We assume vertices start at zero
    vector_t center;
    vector_t bb;

    group_t *groups;
    vertex_t *vertices;
    uint32_t *indices;

    uint32_t num_vertices;
    uint32_t num_indices;

    file_asset_t *file_asset;
};

struct group {
    char *name;
    material_t *material;

    uint64_t index_base;
    uint32_t num_indices;
    uint32_t pad;

};


model_t *model_load(assman_t *assets, char *name);
model_t *model_lookup(assman_t *assets, char *name);
model_t *model_create(assman_t *assets, char *name, vertex_t *vertices, uint32_t num_vertices, uint32_t *indices, uint32_t num_indices, material_t *material);
void model_destroy(model_t *m);

group_t *model_groupByName(model_t *m, char *name);

void model_draw(int pass, scene_t *scene, void *data);
void model_draw_points(int pass, scene_t *scene, void *data);
void model_draw_lines(int pass, scene_t *scene, void *data);
void model_draw_wireframe(int pass, scene_t *scene, void *data);

void group_draw(int pass, scene_t *scene, void *data);
void group_draw_points(int pass, scene_t *scene, void *data);
void group_draw_lines(int pass, scene_t *scene, void *data);




#endif
