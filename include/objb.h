#ifndef __OBJB_H__
#define __OBJB_H__

#include <stdlib.h>

#define OBJB_NAME_SZ 128
// Magic number: 'OBJb'
#define OBJB_MAGIC  0x4f424a62

#define OBJB_VERSION(maj, min)  (((min)&0xFFFF) | (((maj)&0xFFFF)<<16))
#define OBJB_VERSION_MAJ(ver) (((ver)>>16) & 0xFFFF)
#define OBJB_VERSION_MIN(ver) (((ver)>>0) & 0xFFFF)

#define OBJB_CURRENT_VERSION  OBJB_VERSION(0,4)
/* OBJB version history:
 *  0.1: first one that worked. non-indexed array of verts
 *  0.2: Added indexing for multiple references to unique verts (po only)
 *  0.3: Added normals, interleaved with positions
 *  0.4: Added texture-coords. Note: as with normals, we currently pay the space
 *       price even when there are none in the source file. Also no way [yet] to know
 *       whether a given file has these additional attributes
 */
extern uint32_t objb_current_version;



typedef struct objb_vertex {
    float position[4];
    float normal[4];
    float tex[2];
} objb_vertex_t;

typedef struct objb_header {
    uint32_t objb_magic;
    uint32_t objb_version;

    uint32_t num_indices;
    uint32_t indices_offset;

    uint32_t num_vertices;
    uint32_t vertices_offset;

    uint32_t num_groups;
    uint32_t groups_offset;

} objb_header_t;

typedef struct objb_group {
    char name[OBJB_NAME_SZ];

    uint32_t index_base;
    uint32_t num_indices;
} objb_group_t;

#endif
