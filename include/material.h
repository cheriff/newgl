#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include <shader.h>
#include <stdint.h>
#include <assets.h>
#include <texture.h>

#include <GLFW/glfw3.h>

typedef struct material material_t;

enum uniform_type {
    UT_NONE = 0,
    UT_BOOL,
    UT_SHORT,
    UT_INT,
    UT_FLOAT,
    UT_DOUBLE,
    UT_MATRIX,
    UT_TEXTURE,
};

typedef struct {
    uint32_t gl_type;
    uint32_t matb_type;
    uint32_t matb_size;
    int location;
    char *name;
    union {
        float *f;
        unsigned int *ui;
        signed int *si;
        texture_t *texture;
    } data;
} uniform_t;

struct material {
    asset_t asset;
    file_asset_t *file_asset;
    shader_t *shader;
    uniform_t *uniforms;
    uint32_t num_uniforms;
    uint32_t pad;
};

void material_activate(scene_t *scene, material_t *mat);
material_t *material_lookup(assman_t *assets, char *name);
material_t *material_load(assman_t *assets, char *name);
void material_destroy(material_t *mat);

#endif
