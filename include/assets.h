#ifndef __ASSETS_H__
#define __ASSETS_H__

#include <stdio.h>

struct asset_magager;
typedef struct asset_magager assman_t;
typedef struct asset asset_t;
typedef struct fileAsset file_asset_t;

typedef void (*release_func)(asset_t *);

typedef enum {
    ASS_ANY = 0, // no special handling
    ASS_FILE,
    ASS_SHADER_PROG,
    ASS_MODEL,
    ASS_MATERIAL,
    ASS_TEXTURE,
} asset_type_t;


struct asset {
    assman_t *manager;
    asset_t *next;
    char *name;
    void *asset_data;
    void *link;
    release_func release;
    asset_type_t type;
    int use;
};

typedef void (*file_change_func)(file_asset_t *);

struct fileAsset {
    asset_t asset;
    size_t size;
    void *file_data;
    file_change_func on_change;
    int fd;
    int pad0;
};

assman_t *assman_init(int argc, char *argv[]);
void *assman_addOverlay(assman_t *am, char *name, void *data, size_t sz);

void assman_destroy(assman_t *ass);

file_asset_t *assman_openFile(assman_t *am, char *name);
void assman_closeFile(file_asset_t *file);

file_change_func assman_setFileOnchange(file_asset_t *file,
        file_change_func change);
int assman_update(assman_t *am);

asset_t *assman_lookup(assman_t *am, char *name, asset_type_t type);
FILE * assman_write_file(assman_t *ass, char *out_path);
int asset_free(asset_t *asset);

void assman_add(assman_t *am, asset_t *asset);

void assman_destroy(assman_t *assets);

void assman_debug_list(assman_t *assets);

#endif
