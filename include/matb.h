#ifndef __MATB_H__
#define __MATB_H__

#include <stdlib.h>

#define MATB_NAME_SZ 128
// Magic number: 'OBJb'
#define MATB_MAGIC  0x4d415462

#define MATB_VERSION(maj, min)  (((min)&0xFFFF) | (((maj)&0xFFFF)<<16))
#define MATB_VERSION_MAJ(ver) (((ver)>>16) & 0xFFFF)
#define MATB_VERSION_MIN(ver) (((ver)>>0) & 0xFFFF)

#define MATB_CURRENT_VERSION  MATB_VERSION(0,1)
/* MATB version history:
 *  0.1: first one that worked.
 */
extern uint32_t matb_current_version;

typedef struct matb_uniform {
    char name[MATB_NAME_SZ];
    uint32_t type;
    uint32_t size;

    // Store it full for now - tightly pack later if necessary
    uint8_t data[sizeof(float) * 16];
} matb_uniform_t;

typedef struct matb_header {
    uint32_t matb_magic;
    uint32_t matb_version;

    char shader[MATB_NAME_SZ];

    uint32_t num_uniforms;
    uint32_t uniforms_offset;
} matb_header_t;

#endif
