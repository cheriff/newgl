#ifndef __FRAMERATE_H__
#define __FRAMERATE_H__

#include <sys/time.h>
#include <assert.h>
#include <string.h>
#include <limits.h>


typedef struct {
    int64_t starttime;
    int64_t framestart;
    int64_t lasttime;
    int64_t max;
    int64_t min;
    int frames;
    int padding;
} FrameTime;

static inline int64_t
timestamp(void)
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return (int64_t)tp.tv_sec*1000000 + (int64_t)tp.tv_usec;
}

static inline void
InitFrameTime(FrameTime *time)
{
    memset(time, 0, sizeof(*time));
    time->starttime = timestamp();
}

static inline float
frameStart(FrameTime *time)
{
    time->framestart = timestamp();
    return (float)((time->framestart - time->starttime)) / 1000000.0f;
}



static inline char *
frameEnd(FrameTime *time)
{
    int64_t now = timestamp();
    int64_t delta = now - time->framestart;
    if (delta > time->max) time->max = delta;
    if (delta < time->min) time->min = delta;

    char *ret = NULL;
    if ((now % 1000000) < (time->lasttime % 1000000) ) {
        static char title[1024];
        snprintf(title, 1024, "FPS: %d. Max[%lld] Min[%lld]",
                time->frames,
                time->max, time->min);
        time->frames = 0;
        time->min = LLONG_MAX;
        time->max = LLONG_MIN;
        ret = title;
    }
    time->frames++;
    time->lasttime = now;
    return ret;
}

#endif
