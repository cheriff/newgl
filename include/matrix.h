#ifndef __MATRIX_H__
#define __MATRIX_H__

#include "matrix.h"

#include <printf.h>

#define VEC(x, y, z) = (vector_t){ .f = {(x), (y), (z), 1} }

extern printf_domain_t matrix_domain;
#define dmprintf(format, ...)\
    xprintf(matrix_domain, NULL, (format), __VA_ARGS__)

typedef struct vector {
    float f[4];
} vector_t;

typedef union {
    float f[16];
    vector_t v[4];
} matrix_t;


void
matrixRotate(matrix_t *m,
             float degrees,
             float x, float y, float z);

void
matrixTranslate(matrix_t *m,
                float x, float y, float z);

void
matrixScale(matrix_t *m,
            float x, float y, float z);

void
matrixIdentity(matrix_t *m);

void
matrixMultiply(matrix_t *restrict c, matrix_t *restrict a, matrix_t *restrict b);

void
matrixPrint(matrix_t *m);

void
vectorPrint(vector_t *v);

void
matrixPerspective(matrix_t *m, float near, float far, float aspect, float fov);


static inline void
vector_add(vector_t *c, vector_t a, vector_t b)
{
    c->f[0] = a.f[0] + b.f[0];
    c->f[1] = a.f[1] + b.f[1];
    c->f[2] = a.f[2] + b.f[2];
    c->f[3] = a.f[3] + b.f[3];
}

static inline void
vector_mul(vector_t *c, vector_t a, float b)
{
    c->f[0] = a.f[0] * b;
    c->f[1] = a.f[1] * b;
    c->f[2] = a.f[2] * b;
    c->f[3] = a.f[3] * b;
}

void matrix_init(void);



#endif
